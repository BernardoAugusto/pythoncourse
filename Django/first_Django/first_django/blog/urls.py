from django.contrib import admin
from django.urls import path
from first_django.blog.views import index, post_list, post_detail

urlpatterns = [
    path('', index),
    path('posts', post.list),
    path('posts/<int:year>/<int:month>/<int:day>/<slug:slug>', post_detail, name="detail")
]