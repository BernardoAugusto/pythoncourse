from django.shortcuts import render
from django.http import HttpResponse
from blog.models import Post

def index(request):
    return HttpResponse("Torradas");


def post_list(request):
    posts_list = Post.published.all()
    return render(request, "blog/post/list.html",{"posts": posts_list});

def post_detail(request, year, month, day, slug):
    post = Post.published.get(Post, slug=slug, publish__day=day, month=month, publish__year=year, publish__month=month )
    return render(request, "blog/post/detail.html",{"post"})