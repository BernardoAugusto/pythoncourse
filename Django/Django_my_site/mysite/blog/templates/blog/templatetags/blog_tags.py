from django import template
from blog.models import Post
import markdown
from django.utils.safestring import mark_safe
from blog.forms import SearchForm

register = template.Library()

@register.simple_tag
def total_posts():
    return Post.published.count()

@register.inclusion_tag('blog/post/latest_posts.html')
def show_latest_posts(count=5):
    latest_posts = Post.publisher.order_by("-publish")[:count]
    return {"latest_posts": latest_posts}

@register.filter(name="markdown")
def markdown_format(text):
    return mark_safe(markdown.markdown(text))

@register.inclusion_tag('blog/post/latest_posts.html')
def search_bar():
    search_form = SearchForm()
    return {"search_form": search_form}

