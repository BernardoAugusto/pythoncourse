from django.shortcuts import render, get_object_or_404, redirect
from django.core.paginator import Paginator, EmptyPage,\
                                  PageNotAnInteger
from django.views.generic import ListView
from .models import Post
from .forms import CommentForm
from taggit.models import Tag
from django.db.models import Count

@login_required
def post_list(request, tag_slug=None):
    object_list = Post.published.all()

    tag=None

    if tag_slug:
        tag = get_object_or_404(Tag, slug=tag_slug)
        object_list = object_list.filter(tags__in=[tag])

    paginator = Paginator(object_list, 3) # 3 posts in each page
    page = request.GET.get('page')
    try:
        posts = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer deliver the first page
        posts = paginator.page(1)
    except EmptyPage:
        # If page is out of range deliver last page of results
        posts = paginator.page(paginator.num_pages)
    return render(request,
                 'blog/post/list.html',
                 {'page': page,
                  'posts': posts,
                  'tag': tag})


def post_detail(request, year, month, day, post):
    post = get_object_or_404(Post, slug=post,
                                   status='published',
                                   publish__year=year,
                                   publish__month=month,
                                   publish__day=day)
    new_comment = None

    if request.method == "POST":
        comment_form = CommentForm(data=request.POST)
        if comment_form.is_valid():
            new_comment = comment_form.save(commit = False)
            new_comment.post = post
            new_comment.save()
            return redirect('post')

    elif request.method == "GET":
        comment_form = CommentForm()

    post_tags_ids = post.tags.values.list('id', flat=True)
    similar_posts = Post.publised.filter(tags__in=[post_tags_ids])\
                                .exclude(id=post.id)
    similar_posts = similar_posts.annotate(same_tags=Count('tags')).order_by('-same_tags','-publish')[:4]

    return render(request,
                  'blog/post/detail.html',
                  {
                      'post': post,
                      'comment_form': comment_form,
                      'new_comment': new_comment,
                      'similar_posts': similar_posts
                   })

class PostListView(ListView):
    queryset = Post.published.all()
    context_object_name = 'posts'
    paginate_by = 3
    template_name = 'blog/post/list.html'

def search(request):
    query = request.GET.get("query")
    title_query = Post.published.filter(title__contains=query)
    body_query = Post.published.filter(body__contains=query)
    results = title_query.union(body_query)
    return render(request,
                  'blog/post/search.html',
                  {'results': results})
