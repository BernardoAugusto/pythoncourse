const App = () => {
  const currentTime = new Date().toLocaleDateString();
  return React.createElement("div", { className: "app" }, [
    Tweet(),
    Tweet(),
    Tweet(),
  ]);
};

const Tweet = () => {
  const styles = { color: "red" };
  return React.createElement("div", null, [
    React.createElement("p", { style: styles }, "Hello World"),
    React.createElement("button", null, "Submit"),
  ]);
};

ReactDOM.render([App()], document.getElementById("root"));
