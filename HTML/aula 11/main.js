"use strict";
$(document).ready(function () {
  var squareIn = true;
  $("#switch1").click(function () {
    if (squareIn) {
      $("#square1").fadeOut("slow");
    } else {
      $("#square1").fadeIn("slow");
    }
    squareIn = !squareIn;
  });
  $("#switch2").click(function () {
    if (squareIn) {
      $("#square2").slideDown("fast");
    } else {
      $("#square2").slideUp("slow");
    }
    squareIn = !squareIn;
  });
  $("#switch3").click(function () {
    if (squareIn) {
      $("#square3").animate({ height: "200pt" }, "fast");
    } else {
      $("#square3").animate({ height: "0" }, "slow");
    }
    squareIn = !squareIn;
  });
});
