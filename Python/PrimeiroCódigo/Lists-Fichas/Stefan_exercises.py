# -*- coding: utf-8 -*-

#ficha 1

#exercise 1

print("Insert temperature:")
temp = float(input())
C = float((temp - 32)/1.8)
print(C,"degrees Fahrenheit")


#exercise 2

print("Insert year of birth:")
day = int(input())
age = 2020-day
print("You are:",age,"years old")


#exercise 3

print("Insert hour here:")
h = float(input())
seconds = int((h*60)*60)
print("There are",seconds,"seconds in",h,"hours")


#exercise 5

x=1
if x == 1:
    x += 1
    if x == 1:        #not executed
        x += 1
    else:
            x -= 1
else:
    x -=1
print(x)


x=0
if x == 1:
    x += 1
    if x == 1:       #not executed
        x += 1
    else:
            x -= 1
else:
    x -=1
print(x)


#exercise 6

print("Insert number:")
a = int(input())
r = ""
R = {1000:"M",500:"VC",100:"C",50:"L",10:"X",9:"IX",5:"V",4:"IV",1:"I"}
l = list(R.keys())

for i in l:
    q = a // i
    if q != 0:
        for _ in range(q):
            r += R[i]
    a = a%i
print(r)


#exercise 7

print("Insert a year:")
year = int(input())
temp = (year // 100)
rest = (year % 100)
if rest != 0:
    print("This year belongs to the:",(temp+1),"th Century")
else:
    print("This year belongs to the:",(temp),"th Century")


#exercise 8

print("Insert date:")    #type "xx-xx-xxxx"
data = str(input())
print("Insert amount of days you wish to move forward:")
x = int(input())
data1 = int(data[0:2])
data2 = int(data[3:5])
data3 = int(data[6:])
data1 += x

if data1 <= 31:
    print(str(data1)+"-"+str(data2)+"-"+str(data3))
if data1 > 31:
    data1 = 31 - data1
    data2 = data2 + 1
    print(str(data1)+"-"+str(data2)+"-"+str(data3))
if data2 > 12:
    print(data2,"is not an existing month")


#exercise 12
    
n = int(input("Escreve um número inteiro: "))
current_n = 1
while current_n <= n:
    i = current_n
    f = 1
    while i > 1 and f < 1000:
        f = f * i
        i = i - 1
        print("Factorial de " + str(current_n) + ": " + str(f))
        current_n = current_n + 1

########################################################

#ficha 2

#exercise 1

def Divisão_Inteira (x,y):
    """
      Imprime a divisão inteira de, x e y, e 
      avisa o usuário em caso de divisão por 0
    ----------
    x : int
        Dividendo.
    y : int
        Divisor.
        
    Returns x//y

    """
    if y == 0:
        print("Divisão por zero")
    else:
        return x//y

Divisão_Inteira(4, 2) #2
Divisão_Inteira(2, 4) #0
Divisão_Inteira(3, 0) #"Divisão por zero"
help(Divisão_Inteira) #prints the name of the function and it's docstring
Divisão_Inteira()     #Error. The function requires at least 2 arguments


#exercise 3

def potência (a,b):
    return a**b
a = 2
b = 3
potência(a,b)
potência(b,a)
potência(2,0)
potência(2)    #Erro pois a função necessita de dois argumentos


def potênciaP (a):       #b)
    return a^a


def potência (a,b):      #c)
    if b == None:
        return a**a
    else:
        return a**b


#exercise 4

a = 4                 #variavel global
def printFunção():    
#a variavel global é "substituida" pela variavel local
    a = 17           
    print("Dentro da função:",a)
printFunção()
print("Fora da função:",a)


#exercise 6
ano = 2020
mes = 10
dia = 28

print("Dados do Pai:")
anoP = int(input("Introduza o ano de nascimento:"))
mesP = int(input("Introduza o mês de nascimento:"))
diaP = int(input("Introduza o dia de nascimento:"))
print("Dados da Mãe:")
anoM = int(input("Introduza o ano de nascimento:"))
mesM = int(input("Introduza o mês de nascimento:"))
diaM = int(input("Introduza o dia de nascimento:"))

if mesP > mes or (mesP == mes and diaP > dia):
    print("O Pai tem",ano-anoP-1,"ano(s)")
else:
    print("O Pai tem",ano-anoP,"ano(s)")

if mesM > mes or (mesM == mes and diaM > dia):
    print("A Mãe tem",ano-anoM-1,"ano(s)")
else:
    print("A Mãe tem",ano-anoM,"ano(s)")


#exercise 7

a = int(input("Insert the 1st number:"))
b = int(input("Insert the 2nd number:"))
def Max(a,b):
    """
    Returns the biggest of
    two inserted numbers
    
    Parameters
    ----------
    a : Int
        
    b : Int
        
    Returns
    -------
    Int
        The biggest number

    """
    return max(a,b)
print(Max(a,b))

def Min(a,b):
    if Max(a,b) == a:
        return b
    else:
        return a
print(Min(a,b))  


#exercise 8

x = str(input("Insert a number:"))
def minimize(x):
    if len(x) == 1:
        print(0)
    else:
        print(x[:2])   
minimize(x)

############################################

#ficha 3

#exercise 1

print(list(map(lambda x: x+1, range(1,4))))  #[2, 3, 4]
print(list(map(lambda x :x>0, [3,-5,-2,0])))  #[True, False, False, False]
print(list(filter(lambda x: x>5, range(1,7))))  #[6]
print(list(filter(lambda x: (x%2)==0, range(1,11))))  #[2, 4, 6, 8, 10]


#exercise 2



############################################

#ficha 4

#exercise1 & #exercise2

from statistics import mean

f = open("temp.txt","r")

def media(ficheiro):
    with open(ficheiro) as file_reader:
        for line in file_reader:
            lines = line.split() #removes both " " and \n
            item = list(map(float,lines))  #turn each item of each line to a float
            print(round(mean(item),2))
media("temp.txt")

def ficheiro_falha():
    user_input = input("Insira o nome do ficheiro:")
    try:
        media(user_input)
    except:
        print("Failure opening file")
ficheiro_falha()

############################################

#ficha 6
 
import numpy as np

def circlePerimeter(radius):
    pi = np.pi
    circle_perimeter = (2*pi)*radius
    print(f"{circle_perimeter:.3f}")

def circleArea(radius):
    pi = np.pi
    circle_area = pi*(radius**2)
    print(f"{circle_area:.3f}")



def squarePerimeter(s):
    square_perimeter = s*4
    print(f"{square_perimeter}")

def squareArea(s):
    square_area = s**2
    print(f"{square_area}")


a = int(input("Insert the square's side length:\n"))
print("The square's perimeter is:")
squarePerimeter(a)
print("The square's area is:")
squareArea(a)

b = int(input("Insert the circle's radius:\n"))
print("The circle's perimeter is:")
circlePerimeter(b)
print("The circle's area is:")
circleArea(b)

##################################################

class Human:
    def __init__(self, name, age, weight, height):
        self.name = name
        self.age = age
        self.weight = weight
        self.height = height
    def growth(self, age, weight, height):
        aging = age + 1
        while aging < 21:
            height += 0.5




Harry = Human("Harry",18,64,1.80)
print("Harry's height:", Harry.height)





