# -*- coding: utf-8 -*-

#Bernardo Augusto

#exercise 1

age = 34
area = 12
credit = 5
songs = ["Angels Forever", "Pawn Shop Blues", "Heart of Glass"]

###############################################################

#exercise 2

text = input()
print(text[::-1])

###############################################################

#exercise 3

s = "abc"

length = len(s)
print(length)


sa = s[0]*3
sb = (s[1]*3)
sc = (s[2]*3)

s2 = (sa + sb + sc)

print(s2)


###############################################################

#exercise 4

s = s2

print("First occurrence of b in position =",s.find("b"))
print("First occurrence of ccc in position =",s.find("ccc"))

print(s.replace("a","X"))
print(s.replace("a","X",1))

###############################################################

#exercise 5

string = "aaa bbb ccc"

x = string.upper()

print(x)

x2 = x.replace("BBB", "bbb")

print(x2)

###############################################################

#exercise 6

a = 10
b = a
c = 9 
d = c
c = c +1
print("a =",a,"b =",b,"c =",c,"d =",d)

###############################################################

#exercise 7

x = 4
y = 20

x,y = y,x

print("x =",x)
print("y =",y)

###############################################################

#exercise 8

print (" Enter a number:")
a = int(input())
b = a%2

if a >= 100:
    if b == 0:
     print ("The number is even and equal or higher than 100")
    if b != 0:
     print ("The number is odd and equal or higher than 100")
if a < 100:
    if b == 0:
        print ("The number is even and smaller than 100")
    if b != 0:
        print ("The number is odd and smaller than 100")




