# -*- coding: utf-8 -*-


import matplotlib.pyplot as plt


CitizenM = {"Preço":7,"Capacidade":3,"Simpatia":2,"Ambiente":5,"Conhecimento":2,
     "Disponibilidade":6,"Conveniência":7,"Simplicidade":6}

TorelPalace = {"Preço":2,"Capacidade":6,"Simpatia":7,"Ambiente":7,"Conhecimento":5,
     "Disponibilidade":5,"Conveniência":2,"Simplicidade":4}

keys1 = CitizenM.keys()   #Atributos
values1 = CitizenM.values() #Nível de importância
keys2 = TorelPalace.keys()   
values2 = TorelPalace.values()

plt.plot(keys1, values1,color = "red", marker = "*", label="CitizenM")
plt.plot(keys2, values2,color = "blue", marker = "+", label="TorelPalace")

plt.legend()
plt.xticks(rotation = 60, horizontalalignment = "center")
plt.title("Curvas de valor")
plt.xlabel("Atributos")
plt.ylabel("Nível de importância")
plt.show()

##############################################################







