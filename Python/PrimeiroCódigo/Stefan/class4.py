# -*- coding: utf-8 -*-

#exercise 4

from statistics import mean

f = open("C:\\Users\\utilizador\\Desktop\\Code\\Pythoncode\\PrimeiroCódigo\\Files\\temp.txt","r")

def ficheiro_falha():
    user_input = input("Insira o nome do ficheiro:")
    try:
        media(user_input)
    except:
        print("Failure opening file")
        
ficheiro_falha()
def media(ficheiro):
    with open(ficheiro) as file_reader:
        for line in file_reader:
            lines = line.split() #removes both " " and \n
            item = list(map(float,lines))  #turn each item of each line to a float
            print(round(mean(item),2))

####################################################

#manipulação de CSV
#exercise 1

import csv

def convert_dot(item1,item2):
    return (float((item1.replace(",","."))),float(item2.replace(",",".")))

def csvLinhaParaGrafico(ficheiro): 
    gráfico = []
    with open(ficheiro,newline="") as ficheiro_csv: #newline substitutes at reading the \n for spaces
        leitor = csv.reader(ficheiro_csv,delimiter = ";")
        line1 = leitor._next_()
        line2 = leitor._next_()
        gráfico.append(convert_dot(line1,line2))
        
csvLinhaParaGrafico("file.csv")

####################################################

class House:
    area = 120
    sides = (2,3)
    def openDoor(self):
        print("opened door")
        
    def __init__(self, cor_das_paredes=0xffffff, cor_do_telhado=0x0000ff): #personalizes the object's layout or attributes
         self.cor_das_paredes = cor_das_paredes
         self.cor_do_telhado = cor_do_telhado

print(House.area)
print(House.open_door) #Functions can only be accessed by class objects unless we're using "pass"

concrete_house = House(0xffffff, 0x0000ff) #0x indicates number is hexadecimal
concrete_house.area = 450  #Changes the value of the arg "area"
concrete_house.portas = 2  #Adds arg "porta" and gives it the value 2
print(concrete_house.cor_das_paredes)
print(concrete_house.cor_do_telhado)
print("number of doors:",concrete_house.portas)
del(concrete_house.portas) #Deletes an arg. This can be used n number of times


class Pais:
     def __init__(self, name, age): 
         self.name = name
         self.age = age

class Eu(Pais):
    def __init__(self, name, age, hobby): 
         super().__init__(name, age)  #calls "class Pais"'s __init__ argument or any other argument
         self.hobby = hobby
eu = Eu("Bernardo", 24, "Watch anime")
print("My name is:", eu.name)
print("My age is:", eu.age)
print("My hobby is:", eu.hobby)

#########################################################

























