# -*- coding: utf-8 -*-

#Bernardo Augusto


#exercicio 7

a = int(input("Insert the 1st number:"))
b = int(input("Insert the 2nd number:"))
def Max(a,b):
    """
    Returns the biggest of
    two inserted numbers
    
    Parameters
    ----------
    a : Int
        
    b : Int
        
    Returns
    -------
    Int
        The biggest number

    """
    return max(a,b)
print(Max(a,b))

def Min(a,b):
    if Max(a,b) == a:
        return b
    else:
        return a
print(Min(a,b))      

############################################

#exercicio 8

x = str(input("Insert a number:"))
def minimize(x):
    if len(x) == 1:
        print(0)
    else:
        print(x[:2])   
minimize(x)

#############################################

#ficha 3
#exercicio 1

m = map(lambda x:x+1, range(1,4))
a = list(m)
print(a)

L = list(filter(lambda x:x>5, range(1,7)))
print(L)

##############################################

#File handling

f = open("blobfile.txt","r")
#Default value. Opens a file for reading, error if the file does not exist
f = open("blobfile.txt","a")
#Opens a file for appending, creates the file if it does not exist. Adds apending to the end of the file
f = open("blobfile.txt","w")
#Opens a file for writing, creates the file if it does not exist. Overwrites the file with the new input
f = open("blobfile.txt","x")
#Creates the specified file, returns an error if the file exists


f = f = open("blobfile.txt","r")
print(f.read(5)) #Reads the 5 first characters
print(f.readline())  #Reads the first line of the file
for x in f:     #By looping through the lines of the file, we can read the whole file, line by line
  print(x)
f.close() #Closing the file
with open("blobfile.txt") as file_reader:  #With "with" we're able to use\read the file without needing to close it
    for line in file_reader:
        print(line)


f= open("blobfile.txt","a")
f.write("Now the file has more content!")
f.close()


f = open("blobfile.txt","r")
print(f.read())


import os
print("This is the absolute path:",os.path.abspath("blobfile.txt"))
print("This is the relative path:",os.path.relpath("blobfile.txt"))
print("The file name is:",os.path.basename("./blobfile.txt"))

if os.path.exists("demofile.txt"):
  os.remove("demofile.txt")
else:
  print("The file does not exist")

import copy
lines_for_writing = []
with open("blobfile.txt") as file_reader:
    lines = file_reader.readlines() #reads the file lines as a list of strings
    lines_for_writing = copy.deepcopy(lines)
    for line in lines_for_writing:
        line = line.replace("s","p")
    print(lines)
    

try:
    int("abc")
except ValueError:
    print("The string must contain number, not letters")
finally:
    print("Thanks for subscribing")













