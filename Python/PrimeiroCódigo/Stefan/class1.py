# -*- coding: utf-8 -*-

#Bernardo Augusto

#exercise 1

print("Insert temperature:")
temp = float(input())
C = float((temp - 32)/1.8)
print(C,"degrees Fahrenheit")

#########################################

#exercise 2

print("Insert year of birth:")
day = int(input())
age = 2020-day
print("You are:",age,"years old")

#########################################

#exercise 3

print("Insert hour here:")
h = float(input())
seconds = int((h*60)*60)
print("There are",seconds,"seconds in",h,"hours")

#########################################

#exercise 4

x=1
if x == 1:
    x += 1
    if x == 1:        #not executed
        x += 1
    else:
            x -= 1
else:
    x -=1
print(x)


x=0
if x == 1:
    x += 1
    if x == 1:       #not executed
        x += 1
    else:
            x -= 1
else:
    x -=1
print(x)

##########################################

#exercicio 5
                          #this code translates an int into a roman number
print("Insert number:")
a = int(input())
r = ""
R = {1000:"M",100:"C",50:"L",10:"X",9:"IX",5:"V",4:"IV",1:"I"}
l = list(R.keys())

for i in l:
    q = a // i
    if q != 0:
        for _ in range(q):
            r += R[i]
    a = a%i
print(r)

###########################################

#exercicio 6

print("Insert a year:")
year = int(input())
temp = (year // 100)
rest = (year % 100)
if rest != 0:
    print("This year belongs to the:",(temp+1),"th Century")
else:
    print("This year belongs to the:",(temp),"th Century")


#############################################

#exercicio 7

print("Insert date:")    #type "xx-xx-xxxx"
data = str(input())
print("Insert amount of days you wish to move forward:")
x = int(input())
data1 = int(data[0:2])
data2 = int(data[3:5])
data3 = int(data[6:])
data1 += x

if data1 <= 31:
    print(str(data1)+"-"+str(data2)+"-"+str(data3))
if data1 > 31:
    data1 = 31 - data1
    data2 = data2 + 1
    print(str(data1)+"-"+str(data2)+"-"+str(data3))
if data2 > 12:
    print(data2,"is not an existing month")


#or

ano = int(input("ano:"))
mes = int(input("mes:"))
dia = int(input("dia:"))
add = int(input("que dia será daqui a este número de dias?"))
meses_dias = {1:31,2:28,3:31,4:30,5:31,6:30,7:31,8:31,9:30,10:31,11:30,12:31}

if mes == 12:
    dias_do_mes = meses_dias[mes]
    if dia + add > dias_do_mes:
        mes = 1
        dia = add - (dias_do_mes-dia)
        ano += 1
    else:
        dia = dia + add
else:
    if ano % 4 == 0 and not (ano % 400 == 0 and ano % 00 == 0):
        dias_do_mes = 0
        if mes == 2:
            dias_do_mes = meses_dias[mes]+1
        else:
            dias_do_mes = meses_dias[mes]
        if dia + add > dias_do_mes:
            mes += 1
            dia = add - (dias_do_mes-dia)
        else:
            dia = dia + add
    else:
        dias_do_mes = meses_dias[mes]
        if dia + add > dias_do_mes:
            mes = 1
            dia = add - (dias_do_mes-dia)
            ano += 1
        else:
            dia = dia + add
print(f"{ano}-{mes}-{dia}")









