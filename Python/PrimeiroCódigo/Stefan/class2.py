# -*- coding: utf-8 -*-

#Bernardo Augusto

def multiplica(número,valor=2):
    return número * valor
multiplica(2)
multiplica(2,3)


a = [["a",2],["b",25],["c",0]]
a.sort()
print(a)

###################################################

a = [["aaadfgadfgadfgsd",2],["bfgsdfadg",25],["cadsgsdf",0]]
def compare(item):
    """
      compares the items by the
      lenght of their
      1st index
    """
    
    return len(item[0])
a.sort(key=compare)   #sorts the items by the length of it's first index
print(a)

#####################################################

def range(start,end,step=1):
    i = start
    while i < end:
        yield i
        i += step
p = range(0,10,2)
for i in p:
    print(i)

#####################################################

def add_two(x):
    return x + 2
def função(lista,função):
    for i in range(len(lista)):
        lista[i] = função(lista[i])
    return lista
lista = [2,5,6,7]
função(lista,add_two)


lista = [2,5,6,7]
def add_two(x):
    return x + 2
a = list(map(add_two,lista))
print(a)


def multiply(a,b):
    return a*b
a = map(multiply,(1,2,3),(2,3,4))
s = list(a)
print(s)

######################################################

d = [1,22,33,2,16]
def lp(x):
    return x < 16
c = list(filter(lp,d))
print(c)

######################################################

x = ("abc","asfjhaskfj","aslkdfi")
y = list(sorted(x,key=len,reverse=True))

######################################################

def big_sum(lista):
    accumulator = type(lista[0])()
    for i in lista:
        accumulator += i
    return accumulator    
a = ["abc","def","ghi"]
b = big_sum(a)
print(b)

c = [["abc","def"],["ghi","jkl"]]
d = big_sum(a)
print(d)

######################################################

x = [1,2,3,4,5]
y = [x+2 for x in x]  #or  result = []   for i in x:
                      #                    result.append(i+2)

#####################################################

g = [[1,2,3],[4,5,6]]
h = [sub_item for item in g for sub_item in item]
print(h)

#####################################################

#exercicio 1

def Divisão_Inteira (x,y):
    """
      Imprime a divisão inteira de, x e y, e 
      avisa o usuário em caso de divisão por 0
    ----------
    x : int
        Dividendo.
    y : int
        Divisor.
        
    Returns x//y

    """
    if y == 0:
        print("Divisão por zero")
    else:
        return x//y

#####################################################

#exercicio 3

def potência (a,b):
    return a**b
a = 2
b = 3
potência(a,b)
potência(b,a)
potência(2,0)
potência(2)    #Erro pois a função necessita de dois argumentos


def potênciaP (a):       #b)
    return a**a


def potência (a,b):      #c)
    if b == None:
        return a**a
    else:
        return a**b

######################################################

#exercicio 6

a = 4                 #variavel global
def printFunção():    #a variavel global é substituida pela variavel local
    a = 17           
    print("Dentro da função:",a)
printFunção()
print("Fora da função:",a)

######################################################

#exercicio 

ano = 2020
mes = 10
dia = 28

print("Dados do Pai:")
anoP = int(input("Introduza o ano de nascimento:"))
mesP = int(input("Introduza o mês de nascimento:"))
diaP = int(input("Introduza o dia de nascimento:"))
print("Dados da Mãe:")
anoM = int(input("Introduza o ano de nascimento:"))
mesM = int(input("Introduza o mês de nascimento:"))
diaM = int(input("Introduza o dia de nascimento:"))

if mesP > mes or (mesP == mes and diaP > dia):
    print("Pai tem",ano-anoP-1,"ano(s)")
else:
    print("Pai tem",ano-anoP,"ano(s)")

if mesM > mes or (mesM == mes and diaM > dia):
    print("Mãe tem",ano-anoM-1,"ano(s)")
else:
    print("Mãe tem",ano-anoM,"ano(s)")

#######################################################

#exercicio 7

a = int(input("Insert the 1st number:"))
b = int(input("Insert the 2nd number:"))
def Max(a,b):
    """
    Returns the biggest of
    two inserted numbers
    
    Parameters
    ----------
    a : Int
        
    b : Int
        
    Returns
    -------
    Int
        The biggest number

    """
    return max(a,b)
print(Max(a,b))

def Min(a,b):
    if Max(a,b) == a:
        return b
    else:
        return a
print(Min(a,b))  
























