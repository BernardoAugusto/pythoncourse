# -*- coding: utf-8 -*-
                      

#Bernardo Augusto
#Aula 3

print(4==5)
print(6>7)
print(15<100)
print("hello" == "hello")
print("hello" == "Hello")
print("dog" != "cat")
print(True == True)
print(True != False)
print(42 == 42.0)
print(42 == "42")
print("apple"=="Apple")
print("apple">"Apple")  
print("A's unicode is" , ord("A") , "and a's unicode is" , ord("a"))

#######################################################################

minimum_age = 18
age = 17

if age < minimum_age:
    print("Please come back in {0} years" .format(minimum_age - age))
elif age == 100:
    print("You shouldn't be driving anymore")
else:
    print("Congrats, you can get your license")

########################################################################

name = input("Insert username:",)
password = str(input("Insert password:",))
if name == "Josi":
   print('Hello Josi')
if password == 'swordfish':
   print('Access granted.')
else:
   print('Wrong password.')

########################################################################

import random
guesses_taken = 0                          #exercise1
number = random.randint(1,20)

while guesses_taken < 5:
    print("Take a guess between 1 and 20:")
    guess = input()
    guess = int(guess)
    guesses_taken = guesses_taken + 1
    
    if guess < number:
        print("Please guess higher")
    if guess > number:
        print("Please guess lower")
    if guess == number:
        print("Congrats, you won")
        break

########################################################################

                     #Comparisons in print()                     

print((4<5) and (5<6))
print((4<5) and (9<6))
print((1==2) or (2==2))
print((2+2==4)and not(2+2==5)and(2*2==2+2))


#######################################################################

print("How old are you?")                  #exercise2
age = int(input())
retirement_age = 65
minimum_age = 18


if age < minimum_age:
      print("You are too young too work, go back to school")
    
if age > retirement_age:
      print("You have worked enough, drink some tea and rest now")
    
if age > minimum_age and age < retirement_age:
    print("Have a nice day at work")


#######################################################################

a = 3
b = 2
if a==5 and b>0: 
     print("a is 5 and',b,'is greater than zero.") 
else: 
     print('a is not 5 or',b,'is not greater than zero.') 



day = "Saturday"
temperature = 30
raining = False

if day == "Saturday" and temperature > 20 and not raining :
    print("Go out")
else:
    print ("Better finishing python programming exercises")



n = 5                       #Loops
while n>0:
    print(n)
    n = n - 1
print("END")
print(n)

n = 5
while n > 0 :
    print("Time")
    print("ticking")
print("Stopped")


while True:
    line = input(">")
    if line == "done":
        break
    print(line)
print("Done")

while True:
    line = input(">")
    if line[0]== "#":
        continue
    if line == "done":
        break
    print(line)
print("Done")

raw_input = input()
while True: 
   line = raw_input(">")
   if line[0] == "#":
        continue
   if line == "done":
        break
   print(line)
print("Done!")

 








