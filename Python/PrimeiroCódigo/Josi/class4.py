# -*- coding: utf-8 -*-


#Bernardo Augusto
#Aula 4

a = 10
b = a
c = 9
d = c
c = c + 1

#####################################################################

for i in [5 , 4 , 3 , 2 , 1]:
    print(i)
print("END!")



for i in ["1-Pão Alentejano\n2-Bolo Lêvedo\n3-Bolo do Caco\n4-Broa\n5-I Want to Leave"]:
    print(i)

number = 0
print("Menu")
for i in ["- Pão Alentejano","- Bolo Lêvedo","- Bolo do Caco","- Broa","- I Want to Leave"]:
    number = number + 1
    print(number,i)
print("Obrigado pela sua preferência")



friends = ["Ana","Filipe","João","Rodrigo"]
for friend in friends:
    print("Congrats on your new job:", friend)

###############################################################################

D = ord("B")
x = 5 + D
y = 0

while True :
    y = (x % 2) + 10 * y
    x = x // 2
    print ("x =", x, "y =", y)
    if x == 0:
        break

while y != 0:
    x = y % 100
    y = y // 10
    print ("x =", x,"y =", y)

###############################################################################


print("Before")
for i in range(2,6):
    print(i)
print("After")


print("Before")
for i in range(15,0,-5):
    print(i)
print("After")


###############################################################################

                                       #exercise 1
                                                          
frase = "The Best of made in Portugal - Hats, Soaps, Shoes, Tiles & Ceramics, Cork"
letters = frase[0]+frase[4]+frase[20]+frase[31]+frase[37]+frase[44]+frase[51]+frase[59]+frase[69]

print(letters)

if letters.isupper():
    print(letters)
    
# for j in letters:
#     print(j)
                                      #to be finished

#adicional
number_of_as = frase.count("a")
print("Number of As:",number_of_as)


###############################################################################
 
                                     #exercise 2
for i in range(0,50,4):
    print(i)


###############################################################################


largest_num = -1
print("Start:", -1)

for i in [9,41,12,3,74,15]:
    if i > largest_num:
        largest_num = i
    print(largest_num, i)    
print("The winner:", largest_num)



loop_counting = 0
print("Start", loop_counting)
for i in [9,41,12,3,74,15]:
    loop_counting = loop_counting + 1
    print(loop_counting,i)
print("End",loop_counting)


###############################################################################

found = False
print("Start", found)
for i in [9,41,12,3,74,15]:
    if i == 3:
      found = True
      print(found, i)    
print("End", found)


#End















