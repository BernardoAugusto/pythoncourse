# -*- coding: utf-8 -*-

#Bernardo Augusto

import matplotlib.pyplot as plt
import pandas as pd
import csv
f = "C:\\Users\\utilizador\\Desktop\\Code\\Pythoncode\\PrimeiroCódigo\\Josi\\DP_LIVE_04112020161547500.csv"

grafico_PT = {}
grafico_boys = {}
grafico_girls = {}

with open(f,"r") as csv_file:
    csv_reader = list(csv.reader(csv_file, delimiter=','))
    for i in csv_reader:
        if i[0] == "PRT":
            grafico_PT.setdefault(i[5],i[6])
            if i[2] == "BOY":
                grafico_boys.setdefault(i[5],i[6])
            elif i[2] == "GIRL":
                grafico_girls.setdefault(i[5],i[6])


print(grafico_PT)
print(grafico_boys)
print(grafico_girls)


keys = grafico_PT.keys()
values = grafico_PT.values()
plt.plot(keys, values,color = "orange", marker = "*", label="Portugal")
plt.legend()
plt.title("Average between 2003 and 2018")
plt.xlabel("Years")
plt.ylabel("PISA Average")
plt.show()

a = int(grafico_girls["2003"])
b = int(grafico_boys["2003"])

values = pd.DataFrame({"averages": [a,b]},
                      index=["Girls", "Boys"])
values.plot(kind="bar")
plt.xticks(rotation = 60, horizontalalignment = "center")
plt.title("Average by Gender")
plt.xlabel("Genders")
plt.ylabel("Averages in the year 2003")














