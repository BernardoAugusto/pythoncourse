# -*- coding: utf-8 -*-

#Bernardo Augusto

                             #Revisions
#list2 exercise1

n1 = input("Insert the first number:")
n2 = input("Insert the second number:")
c = input("Insert the operator:")

str = n1+c+n2

print(f'\n{n1} {c} {n2}')
res = eval(str)
print("\nRESULT :",res)

############################################

      #Conditionals

day = input("What day is it?\t")
temp = int(input("What's the temperature outside?\t"))
raining = True

if day == "Saturday" and temp > 20 or not raining:
    print("Go out and have fun!")
else:
    print("Better finishing python programming exercises")

##############################################

# import sqlite3                  #unused
# from sqlite3 import Error       #unused
# from numpy import *             #is not reccomended
import numpy as np

b = [4,6,7]
print(np.add(b,5)) #[9,11,12]

print(dir(np))

L = [3, True, "ali", 2.7, [5,8]]

a = [3, [109,27],4,15]
print(a[1]) #print(a(1)) isn't eligible because "a" is a list

a = [1,3,6,5,3]
print(a.count(3))  #2

a = [7,5,30,2,6,25]
print(a[1:4])  #[5,30,2]

a = []
for i in range(4):
    a.append(i)
print(a)   #[0,1,2,3]

###############################################

t = ("English","History","MAthematics")
print(t[1])                #"History
print(t.index("English"))  #0

t = (1,9,3,9)
print(t.count(9))  #2
print(max(t))      #9
print(t+t)         #(1, 9, 3, 9, 1, 9, 3, 9)

print((1,2)==(2,1)) #False

t = (4,7,2,9)
x = list(t)
x.remove(2)
t = tuple(x)
print(t)     #(4,7,9)


a =(1,2)
b = (3,4)
c = zip(a,b)
x = list(c)
print(x)     #[(1, 3), (2, 4)]

print(x[0])   #(1, 3)
z = ((1,3),(2,4))
u = zip(*z)
print(list(u))   #[(1, 2), (3, 4)]

#################################################

d = {"brand":"cherry","model":"arizo5","color":"white"}
d["color"] = "black"
print(d)    #{'brand': 'cherry', 'model': 'arizo5', 'color': 'black'}

x = d.get("model")
print(x)      #arizo5

print(list(d.keys()))   #['brand', 'model', 'color']
print(list(d.values())) #['cherry', 'arizo5', 'black']
d.pop("model")
print(d)  #{'brand': 'cherry', 'color': 'black'}

d = {"ali":[12,13,8],"sara":[15,7,14],"taha":[5,18,13]}
d = {k:sorted(v) for k,v in d.items()}
print(d)   #{'ali': [8, 12, 13], 'sara': [7, 14, 15], 'taha': [5, 13, 18]}

k = ["red","green"]
v = ["#FF0000","#008000"]
z = zip(k,v)
d = dict(z)
print(d)       #{'red': '#FF0000', 'green': '#008000'}

##################################################

f = {"apple","orange","banana"}
f.add("cherry")
print(f)  #{'apple', 'banana', 'cherry', 'orange'}

f.update(["mango","grapes"])
print(f)  #{'orange', 'grapes', 'cherry', 'mango', 'apple', 'banana'}

f.remove("apple")
print(f) #{'orange', 'grapes', 'cherry', 'mango', 'banana'}

X = {1,2,3}
Y = {2,3,4}
print(X.union(Y)) #{1, 2, 3, 4}
print(X | Y) #{1, 2, 3, 4}
print(X.intersection(Y)) #{2, 3}
print(X&Y) #{2, 3}

A = {1,2,4}
B = {1,2,3,4,5}
print(A.issubset(B)) #True
print(B.issubset(A)) #False

##################################################

                         #File Handling

import os

file_path = os.path.abspath("Epopeia.txt")
epopeia = open(file_path,"r")
for line in epopeia:
    print(line)
    if "tempestade" in line.lower():
        print(line)
epopeia.close()

with open(os.path.abspath("Epopeia.txt"),"r") as epopeia:
    for line in epopeia:
        if "tempestade" in line.lower():
            print(line)
print(line)               #the text is printed but it has errors

##################################################

import os

file_path = os.path.abspath("Epopeia.txt")
encoding = "utf-8"

with open(file_path,"r",encoding = "utf-8") as epopeia:
    line = epopeia.readline()
    while line:
        print(line,end="")
        line = epopeia.readline()



with open(file_path,"r",encoding = "utf-8") as epopeia:
    poem = epopeia.readlines()
    while poem:
        print(type(poem)) #poem is printed as a list with each sentence as an argument
        break
print(poem)

for linea in poem[::-1]:  #the order of the sentences is backwards
    print(linea,end = "")
print(type(poem))




with open(file_path,"r",encoding = "utf-8") as epopeia:
    text = epopeia.read()
    while poem:
        print(type(text))   #text is printed as a string
        break
print(text)

for linea in text[::-1]:  #the text and all the words are printed backwards
    print(linea,end = "")
print(type(text))

######################################################

import json
import os

file = open(os.path.abspath("Json"),"r")

with file as json_file:
    jsonObject = json.load(json_file)    #alike read() with .txt, .load is used to read json files
    name = jsonObject["data"][0]["name"]
    address = jsonObject["data"][0]["address"]["city"]

    print(jsonObject)
    print(name)
    print(address)

    a = json.dumps(jsonObject, indent=4)
    print(a)

file = open(os.path.abspath("Json"),"r")


with file as json_file:
    jsonObject = json.load(json_file)
    name = jsonObject["data"][1]["name"]
    address = jsonObject["data"][1]["address"]["city"]

    print(jsonObject)
    print(name)
    print(address)

file = open(os.path.abspath("Json"),"r")


with file as json_file:
    jsonObject = json.load(json_file)
    postal_code = jsonObject["data"][2]["address"]["postal_code"]
    position = jsonObject["data"][2]["position"]

    print(jsonObject)
    print(postal_code)
    print(position)

########################################################



########################################################

import pandas as pd
import os

a = os.path.abspath("Employee_data.xlsx")
all_data = pd.read_excel(a, index_col = 1)


email = all_data["email"].head()
company_name = all_data["company_name"]
xl = pd.ExcelFile(a)
df = xl.parse("b")














