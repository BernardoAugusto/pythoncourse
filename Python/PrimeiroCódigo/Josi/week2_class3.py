# -*- coding: utf-8 -*-
#Bernardo Augusto


for i in range(5):
    for j in range(6):
        print("Josi")
    print("Bernardo")

##############################

import numpy as np
import pandas as pd

print(dir(pd))
# print(pd._version_)

x = np.array([[-1,3],[4,2]])
y = np.array([[1,2],[3,4]])
z = np.dot(x,y)
print(z)

print(pd.Series([0.25,0.5,0.75,1.0]))
data = pd.Series([0.25,0.5,0.75,1.0],
                 index = ["a","x","y","z"])
print(data.values)
data

###############################

import numpy as np
import pandas as pd

population_dict = {"California":38332521,
                   "Texas":123568794,
                   "New York":97435698,
                   "Florida":123547892,
                   "Illinois":256369874}

population = pd.Series(population_dict)
print(population)
population["California":"Florida"]

area_dict = {"California":78645464,"Texas":75369814,"New York":723698521,"Florida":652398741,"Illinois":9672348915}
area = pd.Series(area_dict)

states = pd.DataFrame({"population":population},
                      {"area":area})
states
states.index
states.columns
pd.DataFrame(population,columns=["population"])

################################

pd.Series([2,4,6])
pd.Series(5, index=[100,200,300])
pd.Series({2:"a",1:"b",3:"c"})
pd.Series({2:"a",1:"b",3:"c"}, index=[3,2])

################################

data = ({"a":i,"b":2*i}
        for i in range(3))
pd.DataFrame(data)

################################

ind = pd.Index([2,3,5,7,11])
print(ind)
print(ind[1])
ind[::2]
print(ind.size,ind.shape,ind.ndim,ind.dtype)


indA = pd.Index([1,2,3,5,7,11])
indB = pd.Index([1,3,5,7,9,11])
indA & indB #intersection
indA | indB #union
indA ^ indB #symmetric difference

















