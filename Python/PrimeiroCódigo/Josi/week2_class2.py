# -*- coding: utf-8 -*-
#Bernardo Augusto

def power_three(x):
    return(x**3)
print(power_three(10))

a = lambda x : x**3
print(a(10))

def larger_num(num1, num2):
    if num1 > num2:
        return num1
    else:
        return num2
print(larger_num(5,7))    

larger_num = lambda num1,num2: num1 if num1 > num2 else num2
print(larger_num(5,7))

#########################################

text = "LISBON IS IN PORTUGAL"

def char_lowercase(x):
    char_lowercase = [char.lower() for char in x]
    return char_lowercase
print(char_lowercase(text))

#########################################

     #Numpy

import numpy as np
print(np.__version__)
print(np.version)
print(dir(np))
print(dir(np.array))
students = np.array([[[1,3,5],[1,1,1],[4.5,4,5],[4.3,4.4,4.6]]],dtype=np.int32)
print(students)
print(students.shape,"\n",
      students.nbytes,"\n",
      students.ndim,"\n",
      students.dtype,"\n",
      students.size,"\n",
      students.data,"\n",
      students.itemsize,"\n")

y = np.array([[-1,3]])
y2 = np.array([[1,2]])
y3 = y + y2
print(y3)
y4 = y - y2
print(y4)
y5 = y * y2
print(y5)

x = np.power(10,4,dtype=np.int8)
print(x)  #16 overflow int8 size
x2 = np.power(10,4,dtype=np.int16)
print(x2)  #correct value of 10000

##########################################

import numpy as np
print(np.arange(10))
print(np.arange(10).reshape(2,5))
print((np.resize(np.arange(10),(2,5))))
print((np.resize(np.arange(10),(2,7))))

d = np.arange(2,5)
print(d)
print(d.shape)
print(d[:,np.newaxis])
print(d[:,np.newaxis].shape)

x = np.array([1,4,3])
y = x                   #dependent on x value
z = np.copy(x)          #independent
x[1] = 2
print(x) 
print(y)
print(z)

dtype = [("name","S12"),("grade",float),("age",int),("email","S10")]
values = [("Joseanne",5,31,"josi_ocroch@hotmail.com"),("Hamed",5,32,"hamed_farkhari@iscte-iul.pt"),("Stefan",5,24,"stefan.freya@gmail.com")]
sorted_data = np.array(values,dtype=dtype)
print(np.sort(sorted_data, order="age"))

##########################################

print(np.zeros((3)))  #3 elements in one line
print(np.zeros((3,4))) #4 elements in each 3 lines
print(np.zeros((2,3,4))) #2 blocks with 4 elements in each 3 lines

b = np.zeros((10,6),dtype=int)
print(b)
print(b.ndim,b.type,b.size)
b = np.zeros((10,10))
print("%d bytes" % (b.size * b.itemsize))
b = np.zeros(10)
b[8] = 1
print(b)


b = np.arange(10,50)
print(b)
print(b.ndim,b.dtype,b.size)
b = np.arange(50)
b = b[::-1]
b = b.reshape(5,10)
print(b)
print(b[4,1])  #5th line, 1st element
print(b[3,:])  #4th line, all elements
print(b[0,2:]) #1st line, all elements starting on the 3rd element
print(b[:1,1:]) #until line 1 from the 2nd element

##########################################

print(np.ones((2,1)))
b = np.arange(50)
b = b.reshape(5,10)
c = np.ones((2,10))
print(np.concatenate((b,c)))

a = np.arange(2,6)
b = np.arange([3,5],[4,6])
print(np.vstack(a))
print(np.hstack(b))

b = np.arange[50]
c = b.astype(np.int16)
print(c)
print(b.ndim, b.dtype, b.size)
print(c.ndim, c.dtype, c.size)

d = np.ones((1,3))
x = np.zeros_like((d))
x1 = np.ones_like((x))
print(d)
print(x)
print(x1)
d = np.eye(3)
print(d)
print(np.empty((1,2)))
a = np.full((2,2),np.inf)
print(a)
print(a.dtype,a.size * a.itemsize)

##########################################

import numpy as np
yesterday = np.datetime64("today") - np.timedelta64(1)
today = np.datetime64("today")
tomorrow = np.datetime64("today") + np.timedelta64(1)
print(yesterday,today,tomorrow)

b = np.arange("2019","2021",dtype="datetime64[D]")
print(b.size,b.ndim)
b = np.arange("2020-06","2020-08",dtype="datetime64[D]")
print(b.size,b.ndim)
b = np.arange("2020-06","2020-08",dtype="datetime64[M]")
print(b.size,b.ndim)

###########################################

import numpy as np
import matplotlib.pyplot as plt

values = np.linspace(-(2*np.pi),2*np.pi,60) 
print(values)  #prints values from -2.pi to 2.pi with the size of 100 elements

cos_values = np.cos(values)
sin_values = np.sin(values)

plt.plot(cos_values, color = "blue", marker = "*")
plt.plot(sin_values, color = "red", marker = "*")
plt.show()

############################################

import numpy as np
import matplotlib.pyplot as plt
from PIL import Image
import os

my_path = os.path.abspath("cat.jpeg")

im = Image.open(my_path)
plt.imshow(im)            #shows jpeg file in folder

im2 = Image.open(my_path)
im_p2 = np.array(im2.convert("P"))
plt.imshow(im_p2)            #changes the default color


im3 = Image.open(my_path)
im_p3 = np.array(im3.convert("L"))
plt.imshow(im_p3)            #changes the default color again



