# -*- coding: utf-8 -*-

#Bernardo Augusto

help(len)     #We can also use "len?" in console
L = [1, 2, 3] #"L.insert?" - "L?" - L.<TAB>

def square(a):
    """ Return the square of a """     # square??
    return a ** 2


import math as m
print(m.pi)
from math import e,pi
print(e,pi)
from os import getcwd as gc
gc()
import pandas as pd
pd
import numpy as np
from matplotlib import pyplot as plt
plt


data = np.array([-20, -3, -2, -1, 0, 1, 2, 3, 4, 5])
plt.boxplot(data)

############################################################

print (" Enter a number:")
a = int(input())
b = a%2

if a >= 100:
    if b == 0:
     print ("The number is even and equal or higher than 100")
    if b != 0:
     print ("The number is odd and equal or higher than 100")
if a < 100:
    if b == 0:
        print ("The number is even and smaller than 100")
    if b != 0:
        print ("The number is odd and smaller than 100")
        
#############################################################

import math
dir(math)

s = "a"
dir(s)

#s.split??    or    split.__doc__    #in console


#############################################################

def my_f():
    """
    

    Parameters
    ----------
    def my_f : TYPE
        DESCRIPTION.

    Returns
    -------
    None.

    """
    print("Python")

#############################################################

#exercise 2

from math import fmod,gcd,fabs
fmod(9,4)
gcd(30,4)
fabs(-4)

print(fmod(9,4),gcd(30,4),fabs(-4))

from random import randint, choice, shuffle
randint(1,5)
choice([1,5])
a=[1,2,3,4]
shuffle(a)

print(randint(1,5),choice([1,5]),a)


#############################################################

from datetime import datetime as dt
now = dt.now()

print(now)
print(now.year)
print(now.month)
print(now.day)

print(dt.today())

today = dt.today()

b = today.minute
m = b%2

print(b,"minutes")
if m == 0:
     print ("This current minute is an even number")
if m != 0:
     print ("This current minute is an odd number")

                       #or
 
for i in range(0, 60, 2):
    if b == i:
        print(i,"This current minute is an even number")
        break
    else:
        print(i,"This current minute is an odd number")












