# -*- coding: utf-8 -*-

#Bernardo Augusto

name = "Bernardo"
age = 24

print('%s is %d years old' % (name, age))
print('{} is {} years old'.format(name, age))
print(f"{name} is {age} years old")
print(name, "is", str(age), "years old") #simplified code

#######################################

print("a2".isidentifier())     #True
print("2a".isidentifier())     #False
print("_myvar".isidentifier()) #True
print("my_var".isidentifier()) #True
print("my-var".isidentifier()) #False
print("my var".isidentifier()) #False
print("my$".isidentifier())    #False
print("my#".isidentifier())    #False

########################################

a = 5
b = 1
print("Five plus one is {a+b}")
print(f"Five plus one is {a+b}") #f() formats a and b according to their values

a = b = c = 5
print(a,b,c)

x = 1
y = 2
y,x = x,y
print(x)
print(y)

########################################

s = "Python Course"
print(type(s))
i = 2
print(type(i))
f = 2.5
print(type(f))
c = 2 + 3j
print(type(c))

print(bool(5))        #True
print(bool(-2))       #True
print(bool("Hamed"))  #True
print(bool(0))        #False
print(bool(""))       #False
print(bool([]))       #False
print(bool({}))       #False
print(bool(()))       #False

b = True
c = 5 < 2
print(type(b))
print(type(c))

l = ["apples","grapes", "oranges"]
t = ("apples","banana", "cherry")
d = {"id":213,"name":"farshid"}
s = {"apples","banana", "cherry"}

########################################

a = int(input("Enter a:"))
b = int(input("Enter b:"))
c = a +b
print(c)

n = 12.5
print("%i"%n)
print("%f"%n)
print("%e"%n)

########################################

list(range(2, 10, 1))
for i in range(2, 10, 1):
    print(i, end = "")

#######################################

name = "Farshid"
v = "aeiou"
c = 0

for i in name:
    if i in v:
        print(i)
        c += 1
print(c)

######################################

for i in range(5):
    if i == 3:
        continue
    else:
        print(i,end="")
        
######################################      
        
        
        
        
        
        