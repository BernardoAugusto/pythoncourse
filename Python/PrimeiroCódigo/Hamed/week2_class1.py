# -*- coding: utf-8 -*-

#Bernardo Augusto

import numpy as np

b=np.array([2, 3, 4])
print(b)
print(b[2])
print(type(b))
print(b.dtype)
print(b.shape)



a_list=[(1,2),(3,4),(5,6)]
print(a_list)
print(a_list[1][1])
print(a_list[0:2])
print(a_list[0:2][1])



b=np.array([(1, 2),(3, 4),(5, 6)])
print(b)
print(b[1][1])
print(b[1,1])
print(b[0:2])
print(b[0:2,1])
print(b[1, :])
print(b[:, 0])
print(b.ndim)
print(b.shape)
print(np.reshape(b,(2, 3)))
print(np.reshape(b,(1,-1)))
print(np.reshape(b,(-1, 1)))
print(b<4)
print((b<4).sum(axis=1))


b2=np.array([(1, 2), [3, 4], (5, 6)])
print(b2)


x=np.array([(1, 2, 3), (4, 5, 6)])
print(x)
y=np.array([(10, 11), (20, 21), (30, 31)])
print(y)
print(x.T)
print(x.T * y) # x and Y are matrixes, Not vectors!
print(x.dot(y))  # matrix product
print(x.shape)
print(y.shape)


print(list(range(6)))
print(np.arange(6))
print(np.arange(1, 9, step=3))
print(np.arange(6).reshape(2,3))
print(np.linspace(1, 10, num=5))
print(np.linspace(0, 2, 9))
print(np.zeros([5, 3], dtype = int))
print(np.ones([3, 2]))
print(np.empty([3, 4]))
print(np.full((2,3),4))
print(np.eye(3))


# universal function
t=np.arange(3)
print(t)
np.sqrt(t)
np.e
np.exp(t) # pow(e, t) 
p=np.array([(1, 5, 3), (8, 2, 6)])
print(p)
print(p.sum(axis=0))
print(p.sum(axis=1))
print(p.min(axis=0))
print(p.max(axis=1))
print(np.sort(p,axis=None))
print(np.sort(p))


#random module

print(np.random.seed(543563))
print(np.random.randint(5))
print(np.random.seed(543563))
print(np.random.randint(4, size=(2, 3)))
print(np.random.rand(5))
print(np.random.rand(3))
print(np.random.uniform(0,4,size=(2,3)))
print(np.random.randn(3))

import matplotlib.pyplot as plt
x=np.sqrt(2.0)*np.random.randn(100000)+5.0
print(plt.hist(x,bins=100))

####################################################

import pandas as pd
import numpy  as np

s=pd.Series([7,15,4,4,7,4],index=['r1','r2','r3','r4','r5','r6'])
print(s)
print(s.r2)
print(s['r2'])
print(s[1])
print(s.values)
print(s.index)
print(s.value_counts())

s2=pd.Series([7,15,4,4,7,4])
print(s2)
print(s2[3])



#DataFrame

df = pd.DataFrame([[1,8],[3,4],[5,9],[7,2],[2,6]], 
                  index= ['R1','R2','R3','R4','R5'], 
                  columns=['C1','C2'])
print(df)
print(df[:3])
print(df['C2']['R2'])
print(df)
print(df.iloc[0:2, 0:1]) 
print(df.values)
print(df[df<5])
print(df.head(3))
print(df.tail(2))
print(df.iloc[2:, :])
print(df.iloc[-2:])
print(df.index)
print(df.columns)

df['C1']['R1'] = 2
print(df)
print(df.T)

df2 = df.drop('R3')
print(df2)
print(df.drop('R3', inplace=True))
print(df.drop('C1',axis=1))
print(df.sort_values(by = 'C1'))
print(df.describe())
print(df['C1'].std())
print(df.C1.std())


m = df.iloc[:, 1].values
print(m)
print(sorted(m))
int((len(m))/4)
print(sorted(m)[int((len(m))/4)-1])


df = pd.DataFrame( { 'x1':[1, 2],  'x2':[3, 4] ,'x3':[5, 6]  }  )
print(df)
data = {'Name': ["Ali", "Sara", "Reza", "Taha"],            # the length of values dhould be the same
        'Location' : ["Yazd", "Tehran", "Shiraz", "Hamedan"],
        'Age' : [35, 28, 55, 5]
       }
df = pd.DataFrame(data)
print(df)
print(df.sort_index(axis=1))
print(df.sort_index(axis=0, ascending=False))
print(df.sort_values(by='Age',ascending=False))
print(df[df.Age < 30])
print(df.iloc[2][1])
print(df.iloc[1]['Age'])
df.index = ['FirstR', 'SecondR', 'ThirdR', 'ForthR']
print(df['Age']['FirstR'])
print(df.Age['FirstR'])
print(df.Age.FirstR)
print(df.iloc[:3, 1:3])
print(df.iloc[[0, 2], :])
print(pd.DataFrame(np.random.randn(10,4)))











