# -*- coding: utf-8 -*-

d = {"key":"value"}
d["key2"] = "value2"
print(d)
print(dir(d))

x = d.get("key")
print(x)

y = d.get("cylinder", False)
print(y)

z = "key" in d
print(z)        #true


print(list(d.keys()))
print(list(d.values()))
print(list(d.items()))

#######################################

c = 1
for k,v in d.items():
    print(k, c,":",v)
    c += 1   
for k,v in d.items():
    print(f'{k,v}')

d={"brand":"cherry", "model":"arizo5","color":"white"}
f = d.popitem()
print(d)
print(f)

d[f[0]] = f[1]
print(d)

#########################################

a = ["x", "y", "x", "z", "y", "x"]
d = {}
for i in a:
    if i not in d:
        d[i] = 1
    else:
        d[i] = d[i] + 1
print(d)

#or

a = ["x", "y", "x", "z", "y", "x"]
d = {}
for i in a:
    d[i] = d.get(i,0) + 1
print(d)


d = {}
for i in range(1,101):
	d.setdefault(i,str(i))
print(d)

##########################################

#exercise 1

b = "abfabdcaa"
c = {}
for i in b:
    c[i] = c.get(i,0) + 1
print(c)


##########################################

#exercise 2

line = "a dictionary is a datastructure\na set is also a datastructure."
L1 = line.split("\n")[0].split(".")[0]
L2 = line.split("\n")[1].split(".")[0]

line = "a dictionary is a datastructure\na set is also a datastructure."
d={}
for i in range(2):
    L=line.split("\n")[i].split(".")[0]
    s=L.split()
    for i in s:
        d[i]=d.get(i,0) + 1
print(d)


#########################################

#exercise 3


d ={"a":4,"b":2,"f":1,"d":1,"c":1}

L = list(d.values())
x = sum(L)
print(x)

#or

s=0
for i in d:
    s +=d[i]
print(s)

###########################################

d ={"a":4,"b":2,"f":1,"d":1,"c":1}

import operator
k = operator.itemgetter(0)
print(sorted(d.items(),key=k))


Num = {"ali":[12,13,8],"sara":[15,7,14],"taha":[5,18,13]}
d = {k:sorted(v) for k,v in Num.items()}
print(d)


d1 = {"x":3,"y":2,"z":1}
d2 = {"w":8,"t":7,"z":5}
d=d1.copy()
d.update(d2)
print(d)
d={}
for i in (d1,d2):
    d.update(i)
print(d)

############################################

#exercise 3

d1 = {"x":3,"y":2,"z":1}
d2 = {"w":8,"t":7,"z":5}

for i,j in d2.items():
    if i in d1:
        d1[i] += d2[i]
    else:
        d1.update({i:j})
print(d1)

###########################################



























