# -*- coding: utf-8 -*-

#Bernardo Augusto

s="Python Course"
x=["o","r"]
c={}
for i in s:
    if i in x:
        c[i] = c.get(i,0) + 1
        print(c)
        
#######################################################

d={"x":3,"y":2,"z":1,"y":4,"z":2}  #Error.Dictionaries don't accept duplicates
r={}
for k,v in d.items():
    if k not in r.keys():
        r[k]=v
print(r)

#######################################################

students = [{"id":123,"name":"Sophia","s":True},
            {"id":378,"name":"William","s":False},
            {"id":934,"name":"Sara","s":True}]
a = students[0]
a["s"]
x = 0
for i in students:
    x += int(i["s"])
print(x)

########################################################

d={"F":0,"B":0}
import random
for i in range(10):
    d[random.choice(list(d.keys()))] += 1
print(d)

########################################################

f = {"apple","orange","banana"}
f.add(("cherry","mango"))     #the add() is only capable of taking in 1 arg
f.update(["watermelon","passionfruit"])
f.remove("banana")
print(f)

########################################################

x = {1,2,3,4}
y = {3,4,5,6}
print(x.union(y))
print(x|y)
x.update(y)
print(x)

print(x.intersection(y))
print(x & y)



A = {1,2,3,4,5}
B = {2,4,6}
print(A-B)
print(A.difference(B))  #difference() does not change the initial variables
print(B-A)
print(B.difference(A))



x = {1,2,3,4}
y = {3,4,5,6}
print(x.symmetric_difference(y))
print(x^y)
print((x.union(y))-(x.intersection(y)))


#########################################################

#exercise1

a = {1,2,3,4,6,9,10}
b = {1,3,4,9,13,14,15}
c = {1,2,3,6,9,11,12,14,15}
d = a & b & c       #a.intersection(b,c)
e = a|b|c           #a.union(b,c)
print("Answer one is:",(c-d))
print("Answer two is:",((a&b)-d))
print("Answer three is:",(e-c))

#########################################################

x = {1,2,3}
y = {2,3,4}
print(x.isdisjoint(y))   #if True: x and y have no intersection 


A = {1,2,3}
B = {1,2,3,4,5}
C = set()
print(A.issubset(B))  #True
print(B.issubset(A))  #False
print(C.issubset(A))  #True

##########################################################

#exercise 2

a = "Python Course"
b = {"a","y","c","o","z"}
c = set(a)
print("Answer is:",b&c)

#########################################################

#exercise 3

d1 = {"a":1,"b":3,"c":2}
d2 = {"a":2,"b":3,"c":1}
e = dict(set(d1.items()) & set(d2.items()))
print(e)

###########################################################

#exercise 4

[a,b] = input()
[c,d] = input()
print("Please define two intervals:")

#to be finished


























