# -*- coding: utf-8 -*-

#Bernardo Augusto

from sklearn import datasets
iris = datasets.load_iris()
print(iris.data.shape)
print(iris.feature_names)
print(iris.target_names)
print(iris.data)
print(type(iris.data))
print(iris.target)


import pandas as pd
df = pd.DataFrame(iris.data, columns=iris.feature_names)
df.head()
df['flower_class']= iris.target
print(df)
x=iris.data[:,[2,3]]
print(x)
x2=iris.data[:,2:4]
print(x2)
if (x.all() == x2.all()):
    print("yes")
else:
    print("No")


import matplotlib.pyplot as plt
y=iris.target
plt.scatter(x[:,0],x[:,1],c=y)

df.head()
df.columns
df.columns = ['sl', 'sw', 'pl', 'pw', 'fclass']
df.head()
plt.scatter(df.pl, df.pw, c=df.fclass)
plt.scatter(df.sl, df.sw, c=df.fclass)


#####################################################

import pandas as pd
import numpy as np

df = pd.DataFrame([ [np.nan, 2, np.nan, 0], 
                    [3, 4, np.nan, 1], 
                    [np.nan, np.nan, np.nan, 5], 
                    [np.nan, 3, np.nan, 4],
                    [np.nan, np.nan, np.nan, np.nan] ],
                    columns=list('ABCD'))
print(df)
print(df.isnull())  #or df.isna()
print(df.isnull().sum())
print(pd.isna(df['A']))
print(df['A'].notna())
print(df.fillna(0))
print(df.fillna(df.mean()))
print(df.fillna(df.mean()['A':'B']))
print(df.fillna(method= 'bfill'))
print(df.fillna(method= 'ffill'))
print(df.fillna(method='bfill').fillna(method='ffill'))
print(df.fillna(value={'A': 0, 'B': 1, 'C': 2, 'D': 3}, limit=1))
print(df.fillna(value={'A': 0, 'B': 1}, limit= 1))
print(df.fillna(value={'A': 0, 'B': 1, 'C': 2, 'D': 3}))
df["C"][1] = 3
df["B"] = df["B"].fillna(method="ffill")
print(df)
print(df.dropna())
print(df.dropna(axis=0))  #removes the rows that contain NaN
print(df.dropna(axis=1))  #removes the columns that contain NaN
print(df['B'].dropna())
print(df.dropna(thresh=2) ) #drop rows that have more than 2 NaN values
print(df.dropna(how='all'))  #only drop rows where all members are NaN
print(df.dropna(subset=['B']))  #drop every row with NaN on its column B



from sklearn.impute import SimpleImputer
imr = SimpleImputer(missing_values=np.nan, strategy='mean')
imr = imr.fit(df)
imputed_data = imr.transform(df.values)
print(df)
print(imputed_data)


##################################################################

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

df = 'C:\\Users\\utilizador\\Desktop\\pythoncode\\PrimeiroCódigo\\Files\\Titanic.csv'

with open(df):
    df = df.read()

print(df.info())
print(df.describe())
df.isnull().sum()
df.isna().sum()
pd.isna(df['Age']).sum()
df['Age'].isna().sum()
df.Age.isna().sum()
df.shape  
df.head(7)
df.Name.apply(lambda x: x.split(',')[1].split('.')[0].strip()).unique()

def f(name):
    if '.' in name:
        return name.split(',')[1].split('.')[0].strip()
    else:
        return 'Unknown'


def g(tt):
    if tt in ['Mr']:
        return 1
    elif tt in ['Master']:
        return 3
    elif tt in ['Ms', 'Mlle', 'Miss']:
        return 4
    elif tt in ['Mrs','Mme']:
        return 5
    else:
        return 2


df['title'] = df['Name'].apply(f).apply(g)
df.head(5)
df['title2'] = df['Name'].transform(lambda x: x.split(',')[1].split('.')[0].strip())
print(pd.crosstab(df['Sex'], df['title2']))

t = pd.crosstab(df['title2'], df['Survived'])
print(t)

tt2= pd.crosstab(df['Sex'], df['Survived'] )
print(tt2)
round(tt2.div(tt2.sum(1), axis=0) * 100, 2)
tt2.sum(1)

t_pct = t.div(t.sum(1).astype(float), axis=0)
t_pct * 100

t_pct.plot(kind='bar',stacked=True, title='Survival Rate by title')
plt.xlabel('title')
plt.ylabel('Survival Rate')

df = df.drop(['PassengerId','Name','Ticket'], axis=1)   
df.head()

#Convert categorical variable into dummy/indicator variables
print(df.Embarked.unique())
edt  = pd.get_dummies(df['Embarked'])
print(edt)
edt.sum(axis = 1).sum()
df['Embarked'].isna().sum()
edt.drop(['S'], axis=1, inplace=True)
df = df.join(edt)
df.drop(['Embarked'], axis=1,inplace=True)
print(df.head(3))

df2 = pd.DataFrame({'Bird' : ['A', 'A', 'B', 'B', 'B'],'Speed' : [380, 370, 24, 26,np.nan]})
print(df2)
df2.groupby(['Bird']).mean()
df2['Speed'] = df2.groupby(['Bird'])['Speed'].transform(lambda x: x.fillna(x.mean()))
print(df2)

df['Age'].isna().sum()
df['Age'].notna().sum()
print(df.info())
df['Age'] = df.groupby(['Pclass'])['Age'].transform(lambda x: x.fillna(x.mean()))
print(df)

df['Cabin'].isna().sum()
df['Cabin'].notna().sum()
df.drop("Cabin",axis=1,inplace=True)
print(df)


s = sorted(df['Sex'].unique())
print(s)
z = zip(s, range(len(s)))
l = list(z)
gm = dict(z)
print(gm)
df['Sex'].head(5)
df['Sex'] = df['Sex'].map(gm).astype(int)
df['Sex'].head(5)


list(zip('abcadefg', range(4), range(6)))

###########################################################################

import pandas as pd
import matplotlib.pyplot as plt
from sklearn import datasets
fig1, ax = plt.subplots()
iris = datasets.load_iris()
df = pd.DataFrame(iris.data, columns= ['sl', 'sw', 'pl', 'pw'])
df['t'] = iris.target
ax.scatter(df.pl, df.pw, c=df.t, cmap=plt.cm.Set1, edgecolor='k');
ax.set(xlabel = 'PL', ylabel = 'PW')


from mpl_toolkits.mplot3d import Axes3D

fig2 = plt.figure() #figsize=(8, 6)
ax = Axes3D(fig2, elev=-150, azim=110)
ax.scatter(df.pl, df.pw, df.sl, c=df.t, cmap=plt.cm.Set1, edgecolor='k', s=40)
ax.set_title("iris")
ax.set_xlabel("PL")
ax.set_ylabel("PW")
ax.set_zlabel("SL")
ax.w_xaxis.set_ticklabels([])
ax.w_yaxis.set_ticklabels([])
ax.w_zaxis.set_ticklabels([])
plt.show()


def rotating(angle):
    ax.view_init(None, angle)
import matplotlib.animation as plta
ani = plta.FuncAnimation(fig2, rotating, 360, 
                               interval=40, blit=False)
plt.show()



from sklearn.decomposition import PCA

fig3 = plt.figure(2, figsize=(8, 6))
ax = Axes3D(fig3, elev=-150, azim=110)
X_reduced = PCA(n_components=3).fit_transform(iris.data)
ax.scatter(X_reduced[:, 0], X_reduced[:, 1], X_reduced[:, 2], c=df.t,
           cmap=plt.cm.Set1, edgecolor='k', s=40)
ax.set_title("First three PCA directions")
ax.set_xlabel("1st eigenvector")
ax.w_xaxis.set_ticklabels([])
ax.set_ylabel("2nd eigenvector")
ax.w_yaxis.set_ticklabels([])
ax.set_zlabel("3rd eigenvector")
ax.w_zaxis.set_ticklabels([])
plt.show()













