# -*- coding: utf-8 -*-

#Bernardo Augusto


print(dir(list))

########################################

a=[5,7,12]

print("The index of 7 is:",a.index(7))

########################################

friends = ["Hamed","Josi","Stefan"]

for f in friends:
    print(f)

l = len(friends)
print(l)

for i in range(l):
    print(friends[i])
    


my_list = [1,2,23,4,"word"]

for i in range((len(my_list))):
    if i == my_list[len(my_list)-2]:
        break
    print(my_list[i], my_list[i+1])
    
########################################

a=[7,5,30,2,6,25]

print(a)

print(a[1:4])

print(a[:3])

print(a[3:])

print(a[3:0:-1])

print(a[::-1])

a[1] = [109,27]
print(a)

print(a[1][1])


###########################################


a = [7,5,30,2,6,25]
M = max(a)

print(a.index(M))
                            #or
m = 0               
for i in a:
    if i > m:
        m = i
print(m)
print(a.index(m))


s = 0
for i in a:
    s = s + i    #or s+=i
print(s)
    

print(a.count(3))

a.insert(2,13)
print(a)

a.remove(13)
print(a)

b = a.pop()
print(a)
print(b)

c = a.pop(1)
print(a)
print(c)

d = a[::-1]
print(d)

a.extend([5,53])
print(a)

k = [9,100]
a.append(k)
print(a)

#############################################################

a.clear()

for i in range(4):
     a.append(i)
print(a)

b = a.copy()

#############################################################

import math
a = [round(math.pi,i) for i in range(5)]
print(a)




a ="$araB$ernardo##$$$$$"

b = list(a)

c = "".join(b)




a = [1,2]
b = [1,4,5]
c = []

for i in a:
    for j in b:
        if i != j:
            c.append((i,j))
print(c)

#############################################################


a = [2.6, float("NaN"), 4.8, 6.9, float("NaN")]
b=[]

import math

for i in a:
    if not math.isnan(i):
        b.append(i)
print(b)



for i in a:
    if  math.isnan(i):
        a.remove(i)
print(b)




for i in range(len(a)):
    if a[i] == 2:
        a.pop(i)

print(a)

#############################################################

























