# -*- coding: utf-8 -*-

#Bernardo Augusto

import statistics
from scipy import stats
import numpy as np
a = [4,36,45,50,75]
b = [1,2,2,3,4,7,9]
c = [6,3,9,6,6,5,9,9,3,1]

print(statistics.mean(a))
print(np.mean(b))
print(np.mean(c))

print(statistics.mode(a))
print(stats.mode(b))
print(stats.mode(c))

print(statistics.median(a))
print(statistics.median(b))
print(np.median(c))

############################################################

a = [9,2,5,4,12,7,8,11,9,3,7,4,12,5,4,10,9,6,9,4]
b = [9,2,5,4,12,7]
print(np.mean(a))
print(np.mean(b))
print(np.std(a))
print(statistics.ptsdev(a))
print(statistics.stdev(a))
print(np.std(b))
print(statistics.stdev(b))
print(np.var(a))
print(statistics.pvariance(a))
print(statistics.variance(a))
print(np.var(b))
print(statistics.variance(b))

############################################################

import matplotlib.pyplot as plt
x = [0,0.5,2]
y = [0,1,4]
plt.plot(x,y,"go--") #"color = "green", marker = "o", linestyle = "--"


x = [1,2,3,4,5]
y = [2,4,6,8,10]
plt.plot(x,y,"r--P",label="y=2x")
plt.legend()
plt.title("test")
plt.xlabel("x")
plt.xlabel("y")

x2 = [1,2,3,4,5]
y2 = [1,4,9,16,25]
plt.subplot(211) #2 rows, 1column, index = 1
plt.plot(x2,y2,"b:D",label="y=x^2")
plt.legend()

plt.savefig("D:/plot.png")

############################################################

import numpy as np
import matplotlib.pyplot as plt
x = np.arange(14)
y = np.sin(x/2)

plt.step(x, y+2, label="pre(default")
plt.plot(x, y+2, "o--", color = "grey", alpha = 0.3)

plt.step(x, y+1, where="mid", label="mid")
plt.plot(x, y+1, "o--", color = "grey", alpha = 0.3)

plt.step(x, y, where="post", label="post")
plt.plot(x, y, "o--", color = "grey", alpha = 0.3)

plt.grid(axis="x", color="0.95")
plt.legend(title="Parameter where:")
plt.title("plt.step(where=...)")
plt.show()

############################################################

import numpy as np
import matplotlib.pyplot as plt
x1 = np.linspace(0.0,5.0)
x2 = np.linspace(0.0,2.0)
y1 = np.cos(2*np.pi*x1)*np.exp(-x1)
y2 = np.cos(2*np.pi*x2)

fig, (ax1,ax2) = plt.subplots(2,1)
fig.suptitle("A tale of 2 subplots")
ax1.plot(x1,y1,"o--")
ax1.set_ylabel("Damped oscillation")
ax2.plot(x1,y1,".-")
ax2.set_xlabel("time (s)")
ax2.set_ylabel("Undamped")
plt.show()

############################################################

import numpy as np
data_x = np.arange(1000,250)
data_y = np.arange(4,1)
fig, ax = plt.subplots(2,2)
fig.set_facecolor("lightgrey")
ax[0,0].plot(data_x, data_y, "r-")
ax[1,0].plot(data_x, data_y, "b-")
fig.delaxes(ax[1,0])
ax[1,1].plot(data_x, data_y, "g-")

############################################################

import matplotlib.pyplot as plt
import numpy as np
x = np.arange(0.0,2.0,0.01)
y = 1 + np.sin(2*np.pi*x)

fig,ax = plt.subplots()
ax.plot(x,y,label="sin($\phi$)")

ax.set(xlabel="time(s)",ylabel="sin(2\u03C0s",title="Python Course")
ax.grid()
ax.legend()
fig.savefig("test.png")
plt.show()

############################################################

data = np.array([-10,-5,-2,-1,0,1,2,3,4])
q1 = np.quantile(data,.25)
print(q1)
q2 = np.quantile(data,.50)
print(q2)
q3 = np.quantile(data,.75)
print(q3)
iqr = q3 - q1
print(iqr)
lv = q1 - 1.5*iqr
print(lv)
hv = q3 + 1.5*iqr
print(hv)

############################################################
import numpy as np
import matplotlib.pyplot as plt
from scipy.stats import norm

class1 = np.array([60,70,80,83,85,87,88,89,90,92,94,95,97,100,110])
class2 = np.array([130,143,150,158,160,170,175,182,185,188,190,200,210,280,300])
plt.boxplot([class1,class2],patch_artist=True)



names = ["A","B","C","D","E"]
values = [5,10,8,3,2]
plt.bar(names,values,color="green")
plt.xlabel("Names")
plt.ylabel("Nº of Student")



x = np.array([-3,-2,-1,0,1,2,3])
y = np.array([9,4,1,0,1,4,9])
plt.scatter(x,y,c="r")



data = np.array([3,3,5,6,7,7,8,9,9,10,10,10,11,12,12,14,15,16,17,18,19,19,20])
plt.hist(data, bins=4,edgecolor="black")
plt.xlabel("Value")
plt.ylabel("Frequency")


x = 100 +15 * np.random.randn(1000)
n, bins, _ = plt.hist(x,bins=20,edgecolor="black", density=1)
y = norm.pdf(bins,100,15)
plt.plot(bins,y,"r")


l=["Python","Java","C++"]
s=[200,150,100]
c=["green","red","yellow"]
plt.pie(s,labels=l,colors=c)
plt.pie(s,labels=l,colors=c,autopct="%1.3f%%")
plt.pie(s,labels=l,colors=c,startangle=120)
plt.pie(s,labels=l,colors=c,explode=(0.1,0,0),shadow=True,autopct="%1.1f%%")

#################################################################

import pandas as pd
df = pd.DataFrame(np.random.rand(10,5),
                  columns=["A","B","C","D","E"])
print(df)
df.plot.box(color={"boxes":"green","whiskers":"red","medians":"blue","caps":"gray"})
df.plot.box()
df.plot.box(vert=False, positions=[1,4,5,6,8])


df = pd.DataFrame(np.random.rand(10,4),columns=["a","b","c","d"])
print(df)
df.plot.bar()
df.plot.bar(stacked=True)
df.plot.barh(stacked=True)


df = pd.DataFrame(np.random.rand(50,4),columns=["a","b","c","d"])
print(df.head())
k = df.plot.scatter(x="a",y="b",color="r",label="Group1")
df.plot.scatter(x="c",y="d",color="b",label="Group2",ax=k)


df = pd.DataFrame(np.random.rand(50,3),columns=["a","b","c"])
df["c"]*=200
df.sort_values(by="a",inplace=True)
df.head(7)
df.plot(kind="line",x="a",y="b",color="green")
df.plot(kind="area")

###########################################################################





















x=np.array([1,2,3,4])
y=np.array([7,8])
a,b = np.meshgrid(x,y)
print(a)
print(b)
print(a.shape)



# import matplotlib.pyplot as plt
# from mpl_toolkits.mplot3d import Axes3D

# x = np.arange(-10,10,0.5)
# y = np.arange(-10,10,0.5)
# X,Y = np.meshgrid(x,y)
# Z = X**2 + Y**2

# fig = plt.figure(figsize=(5,5))
# ax = fig.gca(projection="3d")
# s = ax.plot_surface(X,Y,Z,cmap = plt.cm.rainbow)
# cset = ax.contour(X,Y,Z,zdir="z",offet)





import pandas as pd
import matplotlib.pyplot as plt
from sklearn import datasets

fig1, ax = plt.subplots()
iris = datasets.load_iris()
df = pd.DataFrame(iris.data,columns=["sl","sw","pl","pw"])
df["t"] = iris.target

ax.scatter(df.pl,df.pw,c=df.t,cmap=plt.cm.Set1,edgecolor="k")
ax.set(xlabel = "PL", ylabel = "PW")



