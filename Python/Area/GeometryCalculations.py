# -*- coding: utf-8 -*-

#Bernardo Augusto

import numpy as np

def squarePerimeter(s):
    square_perimeter = s*4
    print(f"{square_perimeter}")

def squareArea(s):
    square_area = s**2
    print(f"{square_area}")

def cubeVolume(s):
    cube_volume = s**3
    print(f"{cube_volume}")


def rectangArea(h,b):
    rectang_area = h*b
    print(f"{rectang_area}")

    
def circlePerimeter(radius):
    pi = np.pi
    circle_perimeter = (2*pi)*radius
    print(f"{circle_perimeter:.3f}")

def circleArea(radius):
    pi = np.pi
    circle_area = pi*(radius**2)
    print(f"{circle_area:.3f}")

def sphereVolume(radius):
    c = 4/3    
    pi = np.pi
    volume_sphere =(c*pi*(radius**3))
    print(f"{volume_sphere:.3f}")  


a = int(input("Insert the square's side length:\n"))
print("The square's perimeter is:")
squarePerimeter(a)
print("The square's area is:")
squareArea(a)

b = int(input("Insert the circle's radius:\n"))
print("The circle's perimeter is:")
circlePerimeter(b)
print("The circle's area is:")
circleArea(b)










