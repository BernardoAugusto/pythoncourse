-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 29, 2021 at 02:01 PM
-- Server version: 10.4.17-MariaDB
-- PHP Version: 7.4.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `project`
--
CREATE DATABASE IF NOT EXISTS `project` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
USE `project`;

-- --------------------------------------------------------

--
-- Table structure for table `avião`
--

CREATE TABLE `avião` (
  `matricula` varchar(30) NOT NULL,
  `nome` varchar(30) NOT NULL,
  `marca` varchar(15) NOT NULL,
  `modelo` varchar(15) NOT NULL,
  `data_aquisição` date DEFAULT NULL,
  `pax_exec` int(2) DEFAULT NULL,
  `pax_tur` int(2) DEFAULT NULL,
  `tripulação_req` int(3) DEFAULT NULL,
  `carga` int(9) DEFAULT NULL,
  `autonomia` int(9) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- RELATIONSHIPS FOR TABLE `avião`:
--

--
-- Dumping data for table `avião`
--

INSERT INTO `avião` (`matricula`, `nome`, `marca`, `modelo`, `data_aquisição`, `pax_exec`, `pax_tur`, `tripulação_req`, `carga`, `autonomia`) VALUES
('P122', 'Pothead', 'Fokker', '70', '1995-02-24', 10, 70, 4, 19000, 3410),
('Q495', 'Queue', 'Comac', 'ARJ21', '2011-11-13', 70, 105, 4, 19500, 3700),
('R752', 'Rude', 'Airbus', 'A380', '2019-09-14', 200, 653, 12, 89200, 15700),
('S832', 'Sass', 'Boeing', '777', '1999-03-21', 160, 390, 8, 113000, 14400),
('T131', 'Tarte', 'Airbus', 'A330', '1996-06-12', 35, 300, 6, 72000, 12300),
('U121', 'Urus', 'Comac', 'ARJ21', '2011-11-13', 70, 105, 4, 19500, 3700),
('V332', 'Volk', 'Sukhoi', 'Super 100', '2007-03-03', 20, 83, 6, 23000, 4578),
('W666', 'Wolf', 'Fokker', '70', '1995-02-24', 10, 70, 4, 19000, 3410),
('X132', 'Xenos', 'Airbus', 'A380', '2019-09-14', 200, 653, 12, 89200, 15700),
('Z123', 'Zeus', 'Boeing', '777', '1999-03-21', 160, 390, 8, 113000, 14400);

-- --------------------------------------------------------

--
-- Table structure for table `cabine`
--

CREATE TABLE `cabine` (
  `ID` int(6) NOT NULL,
  `nome` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- RELATIONSHIPS FOR TABLE `cabine`:
--   `ID`
--       `tripulação` -> `ID`
--

--
-- Dumping data for table `cabine`
--

INSERT INTO `cabine` (`ID`, `nome`) VALUES
(1, 'Leandro'),
(2, 'Atanas'),
(3, 'Julia'),
(4, 'Mathilde'),
(5, 'Matias'),
(6, 'Daniela'),
(7, 'Lyana'),
(8, 'Cristina'),
(9, 'Patrício'),
(10, 'Estevão');

-- --------------------------------------------------------

--
-- Table structure for table `comandante`
--

CREATE TABLE `comandante` (
  `ID` int(30) NOT NULL,
  `name` char(30) NOT NULL,
  `horas_comando` float(6,2) DEFAULT NULL,
  `data_promoção` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- RELATIONSHIPS FOR TABLE `comandante`:
--   `ID`
--       `tripulação` -> `ID`
--

--
-- Dumping data for table `comandante`
--

INSERT INTO `comandante` (`ID`, `name`, `horas_comando`, `data_promoção`) VALUES
(1, 'Bruno', 6324.21, '2002-03-12'),
(2, 'Domingos', 9974.28, '1991-12-19'),
(3, 'Felisberta', 8853.01, '1996-09-29'),
(4, 'Idália', 7345.21, '1998-08-22'),
(5, 'Alfredo', 9724.22, '1996-06-16'),
(6, 'Stephen', 2465.20, '2000-05-29'),
(7, 'Dennis', 7846.95, '1989-05-19'),
(8, 'Atílio', 9565.67, '1999-05-04'),
(9, 'Mila', 7861.48, '1998-05-09'),
(10, 'Amir', 287.32, '1990-05-27');

-- --------------------------------------------------------

--
-- Table structure for table `co_piloto`
--

CREATE TABLE `co_piloto` (
  `ID` int(6) NOT NULL,
  `nome` char(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- RELATIONSHIPS FOR TABLE `co_piloto`:
--   `ID`
--       `pilotos` -> `ID`
--

--
-- Dumping data for table `co_piloto`
--

INSERT INTO `co_piloto` (`ID`, `nome`) VALUES
(1, 'Adolfo'),
(2, 'Clara'),
(3, 'Estevão'),
(4, 'Gilberto'),
(5, 'Heládio'),
(6, 'Éder'),
(7, 'Melody'),
(8, 'Osvaldo'),
(9, 'Kushal'),
(10, 'Rahyssa');

-- --------------------------------------------------------

--
-- Table structure for table `escala`
--

CREATE TABLE `escala` (
  `Código_ap` varchar(15) NOT NULL,
  `Designação_local` varchar(100) DEFAULT NULL,
  `Nome_País` char(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- RELATIONSHIPS FOR TABLE `escala`:
--

--
-- Dumping data for table `escala`
--

INSERT INTO `escala` (`Código_ap`, `Designação_local`, `Nome_País`) VALUES
('BCN', 'Barcelona', 'Espanha'),
('BER', 'Berlim', 'Alemanha'),
('DUB', 'Dublin', 'Irlanda'),
('LHR', 'London-Heathrow', 'Inglaterra'),
('MAD', 'Madrid', 'Espanha'),
('MXP', 'Milão-Malpensa', 'Itália'),
('ORL', 'Paris-Orly', 'França'),
('POR', 'Porto', 'Portugal'),
('ROM', 'Roma', 'Itália'),
('WRS', 'Varsóvia', 'Polónia');

-- --------------------------------------------------------

--
-- Table structure for table `manutenção`
--

CREATE TABLE `manutenção` (
  `data` date NOT NULL,
  `nºmilhas` int(30) NOT NULL,
  `horas_voo` float(15,2) NOT NULL,
  `nºaterragens` int(6) NOT NULL,
  `situações_verificadas` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- RELATIONSHIPS FOR TABLE `manutenção`:
--

--
-- Dumping data for table `manutenção`
--

INSERT INTO `manutenção` (`data`, `nºmilhas`, `horas_voo`, `nºaterragens`, `situações_verificadas`) VALUES
('2010-02-13', 56, 23.02, 876, 'Asa partiu'),
('2028-08-26', 2345, 76.24, 568, 'Ficou sem janelas'),
('2015-11-23', 7858, 77.36, 545, 'Passou o filme dos Avengers'),
('2029-07-12', 28975, 74.63, 45, 'Balas por todo o lado'),
('2023-10-14', 5678, 18.04, 5679, 'O Bernardo era um dos passageiros'),
('2020-12-25', 23546, 56.25, 45, 'Ficou sem volante'),
('2018-07-17', 2222, 83.24, 237, 'Entrou em chamas'),
('2013-09-30', 479, 98.03, 86679, 'Alguém gritou bomba but they actually meant it'),
('2011-01-17', 18459, 22.55, 245, 'Bébé a bordo'),
('2015-03-29', 456, 11.43, 2456, 'Hunger Games');

-- --------------------------------------------------------

--
-- Table structure for table `pessoal companhia`
--

CREATE TABLE `pessoal companhia` (
  `Departamento` char(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- RELATIONSHIPS FOR TABLE `pessoal companhia`:
--

--
-- Dumping data for table `pessoal companhia`
--

INSERT INTO `pessoal companhia` (`Departamento`) VALUES
('Comercial'),
('Controlo Aéreo'),
('Finanças'),
('Front-Office'),
('Ground Force'),
('Legislativo'),
('Manutenção'),
('Marketing'),
('Reservas'),
('Stefan');

-- --------------------------------------------------------

--
-- Table structure for table `pilotos`
--

CREATE TABLE `pilotos` (
  `ID` int(6) NOT NULL,
  `horas_voo` float(6,2) DEFAULT NULL,
  `aterragens` int(6) DEFAULT NULL,
  `descolagens` int(6) DEFAULT NULL,
  `n_licença` int(15) DEFAULT NULL,
  `data_licença` date DEFAULT NULL,
  `tipo_avião` varchar(15) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- RELATIONSHIPS FOR TABLE `pilotos`:
--   `ID`
--       `tripulação` -> `ID`
--

--
-- Dumping data for table `pilotos`
--

INSERT INTO `pilotos` (`ID`, `horas_voo`, `aterragens`, `descolagens`, `n_licença`, `data_licença`, `tipo_avião`) VALUES
(1, 29.05, 720, 720, 1984273, '1991-02-25', 'Comercial'),
(2, 43.61, 1423, 1422, 124167, '1997-03-15', 'Comercial'),
(3, 23.23, 600, 591, 123258, '1998-02-25', 'Comercial'),
(4, 98.73, 4129, 5121, 912371, '1983-09-13', 'Carga'),
(5, 15.05, 835, 823, 1984273, '1996-12-05', 'Carga'),
(6, 34.67, 835, 823, 1984273, '1996-12-05', 'Comercial'),
(7, 86.32, 835, 823, 1984273, '1996-12-05', 'Carga'),
(8, 49.24, 835, 823, 1984273, '1996-12-05', 'Comercial'),
(9, 43.79, 835, 823, 1984273, '1996-12-05', 'Comercial'),
(10, 97.27, 835, 823, 1984273, '1996-12-05', 'Carga');

-- --------------------------------------------------------

--
-- Table structure for table `rota`
--

CREATE TABLE `rota` (
  `cidade_inicial` char(30) NOT NULL,
  `cidade_final` char(30) DEFAULT NULL,
  `milhas` int(8) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- RELATIONSHIPS FOR TABLE `rota`:
--

--
-- Dumping data for table `rota`
--

INSERT INTO `rota` (`cidade_inicial`, `cidade_final`, `milhas`) VALUES
('Aveiro', 'Guatemala', 4961),
('Braga', 'Estocolmo', 1793),
('Castelo Branco', 'Amesterdão', 1118),
('Coimbra', 'Paris', 1074),
('Évora', 'Sri Lanka', 5669),
('Lisboa', 'Nova Iorque', 3368),
('Mafra', 'Oslo', 1643),
('Odivelas', 'Madrid', 302),
('Porto', 'Saint Louis', 2315),
('Sintra', 'Londres', 1353);

-- --------------------------------------------------------

--
-- Table structure for table `serviço outsourcing`
--

CREATE TABLE `serviço outsourcing` (
  `Empresa` varchar(30) NOT NULL,
  `Data_entrega` date DEFAULT NULL,
  `Tempo_manutenção` float(10,2) DEFAULT NULL,
  `Preço` float(20,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- RELATIONSHIPS FOR TABLE `serviço outsourcing`:
--

--
-- Dumping data for table `serviço outsourcing`
--

INSERT INTO `serviço outsourcing` (`Empresa`, `Data_entrega`, `Tempo_manutenção`, `Preço`) VALUES
('ArranjaTU', '2021-12-21', 983.00, 63628.00),
('DNuraka', '2021-12-21', 983.00, 63628.00),
('EsperançaQAnde', '2025-06-21', 164372.00, 1300.99),
('FartoDT', '2022-05-12', 1251.00, 73628.00),
('Foise', '2021-08-01', 3251.00, 532683.00),
('NAOvoltou', '2022-05-12', 1251.00, 73628.00),
('NOSinventamos', '2021-11-11', 2624.00, 982465.00),
('NOStentamos', '2021-08-01', 3251.00, 532683.00),
('PartiuDVEZ', '2023-12-30', 11251.00, 754797.00),
('PrayTGod', '2021-11-11', 2624.00, 982465.00);

-- --------------------------------------------------------

--
-- Table structure for table `tripulação`
--

CREATE TABLE `tripulação` (
  `ID` int(6) NOT NULL,
  `Nome` varchar(30) NOT NULL,
  `Idade` int(3) DEFAULT NULL,
  `Sexo` varchar(9) DEFAULT NULL,
  `Morada` varchar(50) DEFAULT NULL,
  `Categoria` varchar(10) NOT NULL,
  `Escalão` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- RELATIONSHIPS FOR TABLE `tripulação`:
--

--
-- Dumping data for table `tripulação`
--

INSERT INTO `tripulação` (`ID`, `Nome`, `Idade`, `Sexo`, `Morada`, `Categoria`, `Escalão`) VALUES
(1, 'Adolfo', 35, 'masculino', 'Avenida da Armada 13 1a', '2', '2'),
(2, 'Bruno', 44, 'masculino', 'Bairro da Bravura 24 3c', '1', '2'),
(3, 'Clara', 28, 'feminino', 'Calçada da Cruz 35', '2', '3'),
(4, 'Domingos', 55, 'masculino', 'Estacada do Douro 47', '1', '1'),
(5, 'Estevão', 24, 'masculino', 'Encosta do Ermo 59', '2', '3'),
(6, 'Felisberta', 47, 'feminino', 'Farol do Feriado 61', '1', '3'),
(7, 'Gilberto', 46, 'masculino', 'Adro dos Gulpilhares 73', '2', '1'),
(8, 'Heládio', 28, 'masculino', 'Boqueirão da Hilda 85', '2', '2'),
(9, 'Idália', 44, 'feminino', 'Cunhal das Ilhas 97', '1', '2'),
(10, 'Juscelina', 59, 'feminino', 'Outeirinho de Janeiras 19', '1', '1');

-- --------------------------------------------------------

--
-- Table structure for table `voo`
--

CREATE TABLE `voo` (
  `Hora_chegada` varchar(30) NOT NULL,
  `Hora_partida` varchar(30) NOT NULL,
  `Data` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- RELATIONSHIPS FOR TABLE `voo`:
--

--
-- Dumping data for table `voo`
--

INSERT INTO `voo` (`Hora_chegada`, `Hora_partida`, `Data`) VALUES
('12am', '7pm', '2024-11-16'),
('12pm', '12am', '2021-01-25'),
('12pm', '7am', '2022-05-12'),
('1am', '9pm', '2017-08-25'),
('3am', '9pm', '2026-09-30'),
('4am', '8pm', '2027-03-23'),
('5am', '1am', '2020-09-26'),
('5pm', '3am', '2022-05-12'),
('5pm', '3pm', '2001-10-12'),
('7am', '12am', '2013-11-07');

-- --------------------------------------------------------

--
-- Table structure for table `voo_cancelado`
--

CREATE TABLE `voo_cancelado` (
  `Hora_chegada` varchar(30) NOT NULL,
  `Hora_partida` varchar(15) NOT NULL,
  `Razão` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- RELATIONSHIPS FOR TABLE `voo_cancelado`:
--   `Hora_chegada`
--       `voo_planeado` -> `Hora_chegada`
--   `Hora_partida`
--       `voo_planeado` -> `Hora_partida`
--

--
-- Dumping data for table `voo_cancelado`
--

INSERT INTO `voo_cancelado` (`Hora_chegada`, `Hora_partida`, `Razão`) VALUES
('12am', '7pm', 'Avião partiu'),
('12pm', '12am', 'Mau tempo'),
('12pm', '7am', 'Is Filipe'),
('1am', '9pm', 'Cliente mudou de ideias'),
('3am', '9pm', 'Covid'),
('4am', '8pm', 'Cliente perdeu voo'),
('5am', '1am', 'Emergência familiar'),
('5pm', '3am', 'Is Stefan'),
('5pm', '3pm', 'Era um terrorista'),
('7am', '12am', 'Emergência médica');

-- --------------------------------------------------------

--
-- Table structure for table `voo_em_curso`
--

CREATE TABLE `voo_em_curso` (
  `Hora_chegada` varchar(30) NOT NULL,
  `Hora_partida` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- RELATIONSHIPS FOR TABLE `voo_em_curso`:
--   `Hora_chegada`
--       `voo` -> `Hora_chegada`
--   `Hora_partida`
--       `voo` -> `Hora_partida`
--

--
-- Dumping data for table `voo_em_curso`
--

INSERT INTO `voo_em_curso` (`Hora_chegada`, `Hora_partida`) VALUES
('1am', '6am'),
('2am', '7am'),
('3am', '3pm'),
('5pm', '6am'),
('6am', '1pm'),
('7am', '5am'),
('7am', '9pm'),
('8pm', '5pm'),
('9pm', '4pm'),
('9pm', '8pm');

-- --------------------------------------------------------

--
-- Table structure for table `voo_planeado`
--

CREATE TABLE `voo_planeado` (
  `Hora_chegada` varchar(30) NOT NULL,
  `Hora_partida` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- RELATIONSHIPS FOR TABLE `voo_planeado`:
--   `Hora_chegada`
--       `voo` -> `Hora_chegada`
--   `Hora_partida`
--       `voo` -> `Hora_partida`
--

--
-- Dumping data for table `voo_planeado`
--

INSERT INTO `voo_planeado` (`Hora_chegada`, `Hora_partida`) VALUES
('12pm', '5pm'),
('1am', '6am'),
('2am', '12am'),
('3am', '7pm'),
('3am', '9pm'),
('4pm', '12am'),
('7am', '5am'),
('8pm', '12pm'),
('9am', '1pm'),
('9pm', '4pm');

-- --------------------------------------------------------

--
-- Table structure for table `voo_realizado`
--

CREATE TABLE `voo_realizado` (
  `Hora_chegada` varchar(30) NOT NULL,
  `Hora_partida` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- RELATIONSHIPS FOR TABLE `voo_realizado`:
--   `Hora_chegada`
--       `voo` -> `Hora_chegada`
--   `Hora_partida`
--       `voo` -> `Hora_partida`
--

--
-- Dumping data for table `voo_realizado`
--

INSERT INTO `voo_realizado` (`Hora_chegada`, `Hora_partida`) VALUES
('11pm', '12am'),
('12pm', '12pm'),
('12pm', '7pm'),
('1am', '11am'),
('1am', '7pm'),
('2am', '12am'),
('3am', '9pm'),
('3pm', '4pm'),
('5am', '5am'),
('9am', '15pm');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `avião`
--
ALTER TABLE `avião`
  ADD PRIMARY KEY (`matricula`,`nome`);

--
-- Indexes for table `cabine`
--
ALTER TABLE `cabine`
  ADD KEY `ID` (`ID`);

--
-- Indexes for table `comandante`
--
ALTER TABLE `comandante`
  ADD KEY `ID` (`ID`);

--
-- Indexes for table `co_piloto`
--
ALTER TABLE `co_piloto`
  ADD KEY `ID` (`ID`);

--
-- Indexes for table `escala`
--
ALTER TABLE `escala`
  ADD PRIMARY KEY (`Código_ap`);

--
-- Indexes for table `pessoal companhia`
--
ALTER TABLE `pessoal companhia`
  ADD PRIMARY KEY (`Departamento`);

--
-- Indexes for table `pilotos`
--
ALTER TABLE `pilotos`
  ADD KEY `ID` (`ID`);

--
-- Indexes for table `rota`
--
ALTER TABLE `rota`
  ADD PRIMARY KEY (`cidade_inicial`);

--
-- Indexes for table `serviço outsourcing`
--
ALTER TABLE `serviço outsourcing`
  ADD PRIMARY KEY (`Empresa`);

--
-- Indexes for table `tripulação`
--
ALTER TABLE `tripulação`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `voo`
--
ALTER TABLE `voo`
  ADD PRIMARY KEY (`Hora_chegada`,`Hora_partida`);

--
-- Indexes for table `voo_cancelado`
--
ALTER TABLE `voo_cancelado`
  ADD PRIMARY KEY (`Razão`),
  ADD KEY `Hora_chegada` (`Hora_chegada`,`Hora_partida`);

--
-- Indexes for table `voo_em_curso`
--
ALTER TABLE `voo_em_curso`
  ADD KEY `Hora_chegada` (`Hora_chegada`,`Hora_partida`);

--
-- Indexes for table `voo_planeado`
--
ALTER TABLE `voo_planeado`
  ADD KEY `Hora_chegada` (`Hora_chegada`,`Hora_partida`);

--
-- Indexes for table `voo_realizado`
--
ALTER TABLE `voo_realizado`
  ADD KEY `Hora_chegada` (`Hora_chegada`,`Hora_partida`);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `cabine`
--
ALTER TABLE `cabine`
  ADD CONSTRAINT `cabine_ibfk_1` FOREIGN KEY (`ID`) REFERENCES `tripulação` (`ID`);

--
-- Constraints for table `comandante`
--
ALTER TABLE `comandante`
  ADD CONSTRAINT `comandante_ibfk_1` FOREIGN KEY (`ID`) REFERENCES `tripulação` (`ID`);

--
-- Constraints for table `co_piloto`
--
ALTER TABLE `co_piloto`
  ADD CONSTRAINT `co_piloto_ibfk_1` FOREIGN KEY (`ID`) REFERENCES `pilotos` (`ID`);

--
-- Constraints for table `pilotos`
--
ALTER TABLE `pilotos`
  ADD CONSTRAINT `pilotos_ibfk_1` FOREIGN KEY (`ID`) REFERENCES `tripulação` (`ID`);

--
-- Constraints for table `voo_cancelado`
--
ALTER TABLE `voo_cancelado`
  ADD CONSTRAINT `voo_cancelado_ibfk_1` FOREIGN KEY (`Hora_chegada`,`Hora_partida`) REFERENCES `voo_planeado` (`Hora_chegada`, `Hora_partida`);

--
-- Constraints for table `voo_em_curso`
--
ALTER TABLE `voo_em_curso`
  ADD CONSTRAINT `voo_em_curso_ibfk_1` FOREIGN KEY (`Hora_chegada`,`Hora_partida`) REFERENCES `voo` (`Hora_chegada`, `Hora_partida`);

--
-- Constraints for table `voo_planeado`
--
ALTER TABLE `voo_planeado`
  ADD CONSTRAINT `voo_planeado_ibfk_1` FOREIGN KEY (`Hora_chegada`,`Hora_partida`) REFERENCES `voo` (`Hora_chegada`, `Hora_partida`);

--
-- Constraints for table `voo_realizado`
--
ALTER TABLE `voo_realizado`
  ADD CONSTRAINT `voo_realizado_ibfk_1` FOREIGN KEY (`Hora_chegada`,`Hora_partida`) REFERENCES `voo` (`Hora_chegada`, `Hora_partida`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
