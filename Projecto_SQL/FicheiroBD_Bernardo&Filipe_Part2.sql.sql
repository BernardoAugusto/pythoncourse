-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 24, 2021 at 01:35 PM
-- Server version: 10.4.17-MariaDB
-- PHP Version: 7.4.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `project`
--

-- --------------------------------------------------------

--
-- Table structure for table `aeroporto`
--

CREATE TABLE `aeroporto` (
  `cod_aeroporto` varchar(3) NOT NULL,
  `local` varchar(20) NOT NULL,
  `pais` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `aeroporto`
--

INSERT INTO `aeroporto` (`cod_aeroporto`, `local`, `pais`) VALUES
('BKK', 'Bangkok', 'Thailand'),
('CDG', 'Paris', 'France'),
('FRA', 'Frankfurt', 'Germany'),
('HND', 'Tokyo-Haneda', 'Japan'),
('LIS', 'Lisboa-Portela', 'Portugal'),
('MAD', 'Madrid-Barajas', 'Spain'),
('OPO', 'Oporto-FrSCarneiro', 'Portugal');

-- --------------------------------------------------------

--
-- Table structure for table `aviao`
--

CREATE TABLE `aviao` (
  `matricula` varchar(6) NOT NULL,
  `nome` varchar(20) NOT NULL,
  `data_aquisicao` date NOT NULL,
  `cod_tipo` varchar(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `aviao`
--

INSERT INTO `aviao` (`matricula`, `nome`, `data_aquisicao`, `cod_tipo`) VALUES
('CS-ABG', 'D. Afonso Henriques', '2015-02-28', 'AB32'),
('CS-LFV', 'D. Filipe', '2015-02-28', 'AB34'),
('CS-PDC', 'D. Fernando', '2015-02-28', 'FK10');

-- --------------------------------------------------------

--
-- Table structure for table `escala`
--

CREATE TABLE `escala` (
  `cod_rota` decimal(5,0) NOT NULL,
  `cod_aeroporto` varchar(3) NOT NULL,
  `n_ordem` decimal(1,0) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `escala`
--

INSERT INTO `escala` (`cod_rota`, `cod_aeroporto`, `n_ordem`) VALUES
('12346', 'FRA', '1'),
('12347', 'FRA', '1'),
('12352', 'FRA', '2'),
('12352', 'MAD', '1'),
('12353', 'FRA', '1'),
('12353', 'MAD', '2'),
('12355', 'CDG', '1'),
('12356', 'CDG', '2'),
('12356', 'LIS', '1'),
('12357', 'LIS', '1');

-- --------------------------------------------------------

--
-- Stand-in structure for view `exercicio1`
-- (See below for the actual view)
--
CREATE TABLE `exercicio1` (
`cod_aeroporto_fim` varchar(3)
,`id` decimal(5,0)
,`data_partida` timestamp
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `exercicio2`
-- (See below for the actual view)
--
CREATE TABLE `exercicio2` (
`nome` varchar(20)
,`COUNT(voo.n_voo)` bigint(21)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `exercicio3`
-- (See below for the actual view)
--
CREATE TABLE `exercicio3` (
`n_voo` decimal(3,0)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `exercicio4`
-- (See below for the actual view)
--
CREATE TABLE `exercicio4` (
`nome` varchar(41)
,`n_licenca` decimal(6,0)
,`min(h.data_licenca)` date
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `exercicio5`
-- (See below for the actual view)
--
CREATE TABLE `exercicio5` (
`nome` varchar(20)
);

-- --------------------------------------------------------

--
-- Table structure for table `habilitado`
--

CREATE TABLE `habilitado` (
  `id` decimal(5,0) NOT NULL,
  `cod_tipo` varchar(4) NOT NULL,
  `n_licenca` decimal(6,0) NOT NULL,
  `data_licenca` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `habilitado`
--

INSERT INTO `habilitado` (`id`, `cod_tipo`, `n_licenca`, `data_licenca`) VALUES
('18200', 'AB32', '34572', '2014-02-11'),
('18200', 'FK10', '34572', '2014-02-12'),
('25100', 'AB32', '34561', '2011-02-13'),
('25100', 'AB34', '34561', '2012-02-12'),
('25100', 'FK10', '34561', '2014-02-13'),
('27900', 'AB32', '34591', '2014-02-22'),
('27900', 'AB34', '34591', '2014-02-27'),
('31100', 'AB32', '34583', '2014-02-13');

-- --------------------------------------------------------

--
-- Table structure for table `piloto`
--

CREATE TABLE `piloto` (
  `id` decimal(5,0) NOT NULL,
  `n_aterragens` decimal(6,0) NOT NULL,
  `n_descolagens` decimal(6,0) NOT NULL,
  `n_horas_voo` decimal(8,0) NOT NULL,
  `tipo` varchar(20) DEFAULT NULL
) ;

--
-- Dumping data for table `piloto`
--

INSERT INTO `piloto` (`id`, `n_aterragens`, `n_descolagens`, `n_horas_voo`, `tipo`) VALUES
('18200', '123', '123', '5490', 'copiloto'),
('25100', '123', '123', '5900', 'comandante'),
('27900', '123', '123', '5940', 'comandante'),
('31100', '123', '123', '1590', 'copiloto');

-- --------------------------------------------------------

--
-- Table structure for table `rota`
--

CREATE TABLE `rota` (
  `cod_rota` decimal(5,0) NOT NULL,
  `cod_aeroporto_ini` varchar(3) DEFAULT NULL,
  `cod_aeroporto_fim` varchar(3) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `rota`
--

INSERT INTO `rota` (`cod_rota`, `cod_aeroporto_ini`, `cod_aeroporto_fim`) VALUES
('12345', 'LIS', 'MAD'),
('12346', 'LIS', 'HND'),
('12347', 'HND', 'LIS'),
('12348', 'MAD', 'LIS'),
('12349', 'LIS', 'OPO'),
('12350', 'OPO', 'LIS'),
('12351', 'OPO', 'MAD'),
('12352', 'LIS', 'BKK'),
('12353', 'BKK', 'LIS'),
('12354', 'LIS', 'CDG'),
('12355', 'LIS', 'BKK'),
('12356', 'OPO', 'HND'),
('12357', 'OPO', 'HND');

-- --------------------------------------------------------

--
-- Table structure for table `tipoaviao`
--

CREATE TABLE `tipoaviao` (
  `cod_tipo` varchar(4) NOT NULL,
  `marca` varchar(20) NOT NULL,
  `modelo` varchar(20) NOT NULL,
  `autonomia` decimal(4,0) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tipoaviao`
--

INSERT INTO `tipoaviao` (`cod_tipo`, `marca`, `modelo`, `autonomia`) VALUES
('AB32', 'Airbus', 'A320', '4000'),
('AB34', 'Airbus', 'A340', '5000'),
('CESS', 'Cessna', '18', '250'),
('FK10', 'Fokker', '100', '3000');

-- --------------------------------------------------------

--
-- Table structure for table `tripulante`
--

CREATE TABLE `tripulante` (
  `id` decimal(5,0) NOT NULL,
  `nome` varchar(20) NOT NULL,
  `apelido` varchar(20) NOT NULL,
  `tipo` varchar(20) DEFAULT NULL
) ;

--
-- Dumping data for table `tripulante`
--

INSERT INTO `tripulante` (`id`, `nome`, `apelido`, `tipo`) VALUES
('18200', 'Carlos', 'Caldas', 'piloto'),
('22300', 'Hilario', 'Hamas', 'pessoal_cabine'),
('23200', 'Bernardo', 'Borges', 'pessoal_cabine'),
('25100', 'Abel', 'Antunes', 'piloto'),
('25600', 'Ilda', 'Ignara', 'pessoal_cabine'),
('27900', 'Gabriela', 'Gusmo', 'piloto'),
('28900', 'Jorge', 'Antunes', 'pessoal_cabine'),
('29200', 'Ernesto', 'Elvas', 'pessoal_cabine'),
('30300', 'Fatima', 'Felgueiras', 'pessoal_cabine'),
('31100', 'Diana', 'Dias', 'piloto');

-- --------------------------------------------------------

--
-- Table structure for table `voo`
--

CREATE TABLE `voo` (
  `n_voo` decimal(3,0) NOT NULL,
  `data_partida` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `cod_rota` decimal(5,0) DEFAULT NULL,
  `id_comandante` decimal(5,0) DEFAULT NULL,
  `id_copiloto` decimal(5,0) DEFAULT NULL,
  `matricula` varchar(6) DEFAULT NULL,
  `data_chegada` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `voo`
--

INSERT INTO `voo` (`n_voo`, `data_partida`, `cod_rota`, `id_comandante`, `id_copiloto`, `matricula`, `data_chegada`) VALUES
('700', '2015-10-31 22:55:12', '12345', '25100', '31100', 'CS-LFV', '2015-11-01 02:21:36'),
('701', '2015-10-31 22:55:12', '12345', '27900', '31100', 'CS-ABG', '2015-11-01 02:21:36'),
('702', '2015-10-31 22:55:12', '12347', '25100', '18200', 'CS-ABG', '2015-11-01 02:21:36'),
('703', '2015-10-31 22:55:12', '12348', '25100', '18200', 'CS-ABG', '2015-11-01 02:21:36'),
('704', '2015-10-31 22:55:12', '12349', '27900', '31100', 'CS-ABG', '2015-11-01 02:21:36'),
('705', '2015-10-31 22:55:12', '12350', '25100', '31100', 'CS-ABG', '2015-11-01 02:21:36'),
('706', '2015-10-31 22:55:12', '12351', '25100', '18200', 'CS-ABG', '2015-11-01 02:21:36'),
('707', '2015-10-31 22:55:12', '12352', '27900', '31100', 'CS-ABG', '2015-11-01 02:21:36'),
('708', '2015-10-31 22:55:12', '12353', '25100', '31100', 'CS-ABG', '2015-11-01 02:21:36'),
('709', '2015-10-31 22:55:12', '12355', '27900', '18200', 'CS-ABG', '2015-11-01 02:21:36'),
('741', '2014-12-01 08:21:59', '12346', '25100', '18200', 'CS-LFV', '2014-12-01 17:21:36'),
('755', '2015-10-12 04:30:32', '12346', '25100', '18200', 'CS-ABG', '2021-02-17 10:35:24'),
('756', '2015-10-12 04:30:32', '12354', '27900', '31100', 'CS-PDC', '2015-10-13 10:32:23'),
('757', '2015-10-12 04:30:32', '12355', '27900', '31100', 'CS-PDC', '2015-10-13 10:32:23'),
('758', '2015-10-12 04:30:32', '12356', '27900', '31100', 'CS-ABG', '2015-10-13 10:32:23'),
('759', '2015-10-12 04:30:32', '12357', '27900', '31100', 'CS-ABG', '2015-10-13 10:32:23');

-- --------------------------------------------------------

--
-- Table structure for table `voopcabine`
--

CREATE TABLE `voopcabine` (
  `n_voo` decimal(4,0) NOT NULL,
  `id_pcabine` decimal(5,0) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `voopcabine`
--

INSERT INTO `voopcabine` (`n_voo`, `id_pcabine`) VALUES
('755', '23200'),
('756', '30300'),
('757', '25600');

-- --------------------------------------------------------

--
-- Structure for view `exercicio1` exported as a table
--
DROP TABLE IF EXISTS `exercicio1`;
CREATE TABLE`exercicio1`(
    `cod_aeroporto_fim` varchar(3) COLLATE utf8mb4_general_ci DEFAULT NULL,
    `id` decimal(5,0) NOT NULL,
    `data_partida` timestamp NOT NULL DEFAULT 'current_timestamp()'
);

-- --------------------------------------------------------

--
-- Structure for view `exercicio2` exported as a table
--
DROP TABLE IF EXISTS `exercicio2`;
CREATE TABLE`exercicio2`(
    `nome` varchar(20) COLLATE utf8mb4_general_ci NOT NULL,
    `COUNT(voo.n_voo)` bigint(21) NOT NULL DEFAULT '0'
);

-- --------------------------------------------------------

--
-- Structure for view `exercicio3` exported as a table
--
DROP TABLE IF EXISTS `exercicio3`;
CREATE TABLE`exercicio3`(
    `n_voo` decimal(3,0) NOT NULL
);

-- --------------------------------------------------------

--
-- Structure for view `exercicio4` exported as a table
--
DROP TABLE IF EXISTS `exercicio4`;
CREATE TABLE`exercicio4`(
    `nome` varchar(41) COLLATE utf8mb4_general_ci NOT NULL DEFAULT '',
    `n_licenca` decimal(6,0) NOT NULL,
    `min(h.data_licenca)` date DEFAULT NULL
);

-- --------------------------------------------------------

--
-- Structure for view `exercicio5` exported as a table
--
DROP TABLE IF EXISTS `exercicio5`;
CREATE TABLE`exercicio5`(
    `nome` varchar(20) COLLATE utf8mb4_general_ci NOT NULL
);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `aeroporto`
--
ALTER TABLE `aeroporto`
  ADD PRIMARY KEY (`cod_aeroporto`);

--
-- Indexes for table `aviao`
--
ALTER TABLE `aviao`
  ADD PRIMARY KEY (`matricula`),
  ADD KEY `fk_Aviao_cod_tipo` (`cod_tipo`);

--
-- Indexes for table `escala`
--
ALTER TABLE `escala`
  ADD PRIMARY KEY (`cod_rota`,`cod_aeroporto`),
  ADD KEY `fk_Escala_cod_aeroporto` (`cod_aeroporto`);

--
-- Indexes for table `habilitado`
--
ALTER TABLE `habilitado`
  ADD PRIMARY KEY (`id`,`cod_tipo`),
  ADD KEY `fk_Habilitado_cod_tipo` (`cod_tipo`);

--
-- Indexes for table `piloto`
--
ALTER TABLE `piloto`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `rota`
--
ALTER TABLE `rota`
  ADD PRIMARY KEY (`cod_rota`),
  ADD KEY `fk_Rota_cod_aeroporto_ini` (`cod_aeroporto_ini`),
  ADD KEY `fk_Rota_cod_aeroporto_fim` (`cod_aeroporto_fim`);

--
-- Indexes for table `tipoaviao`
--
ALTER TABLE `tipoaviao`
  ADD PRIMARY KEY (`cod_tipo`);

--
-- Indexes for table `tripulante`
--
ALTER TABLE `tripulante`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `voo`
--
ALTER TABLE `voo`
  ADD PRIMARY KEY (`n_voo`),
  ADD KEY `fk_Voo_cod_rota` (`cod_rota`),
  ADD KEY `fk_Voo_id_comandante` (`id_comandante`),
  ADD KEY `fk_Voo_copiloto` (`id_copiloto`),
  ADD KEY `fk_Voo_matricula` (`matricula`);

--
-- Indexes for table `voopcabine`
--
ALTER TABLE `voopcabine`
  ADD PRIMARY KEY (`n_voo`,`id_pcabine`),
  ADD KEY `fk_VooPcabine_id_pcabine` (`id_pcabine`);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `aviao`
--
ALTER TABLE `aviao`
  ADD CONSTRAINT `fk_Aviao_cod_tipo` FOREIGN KEY (`cod_tipo`) REFERENCES `tipoaviao` (`cod_tipo`);

--
-- Constraints for table `escala`
--
ALTER TABLE `escala`
  ADD CONSTRAINT `fk_Escala_cod_aeroporto` FOREIGN KEY (`cod_aeroporto`) REFERENCES `aeroporto` (`cod_aeroporto`),
  ADD CONSTRAINT `fk_Escala_cod_rota` FOREIGN KEY (`cod_rota`) REFERENCES `rota` (`cod_rota`);

--
-- Constraints for table `habilitado`
--
ALTER TABLE `habilitado`
  ADD CONSTRAINT `fk_Habilitado_cod_tipo` FOREIGN KEY (`cod_tipo`) REFERENCES `tipoaviao` (`cod_tipo`),
  ADD CONSTRAINT `fk_Habilitado_id` FOREIGN KEY (`id`) REFERENCES `piloto` (`id`);

--
-- Constraints for table `piloto`
--
ALTER TABLE `piloto`
  ADD CONSTRAINT `fk_Piloto_id` FOREIGN KEY (`id`) REFERENCES `tripulante` (`id`);

--
-- Constraints for table `rota`
--
ALTER TABLE `rota`
  ADD CONSTRAINT `fk_Rota_cod_aeroporto_fim` FOREIGN KEY (`cod_aeroporto_fim`) REFERENCES `aeroporto` (`cod_aeroporto`),
  ADD CONSTRAINT `fk_Rota_cod_aeroporto_ini` FOREIGN KEY (`cod_aeroporto_ini`) REFERENCES `aeroporto` (`cod_aeroporto`);

--
-- Constraints for table `voo`
--
ALTER TABLE `voo`
  ADD CONSTRAINT `fk_Voo_cod_rota` FOREIGN KEY (`cod_rota`) REFERENCES `rota` (`cod_rota`),
  ADD CONSTRAINT `fk_Voo_copiloto` FOREIGN KEY (`id_copiloto`) REFERENCES `piloto` (`id`),
  ADD CONSTRAINT `fk_Voo_id_comandante` FOREIGN KEY (`id_comandante`) REFERENCES `piloto` (`id`),
  ADD CONSTRAINT `fk_Voo_matricula` FOREIGN KEY (`matricula`) REFERENCES `aviao` (`matricula`);

--
-- Constraints for table `voopcabine`
--
ALTER TABLE `voopcabine`
  ADD CONSTRAINT `fk_VooPcabine_id_pcabine` FOREIGN KEY (`id_pcabine`) REFERENCES `tripulante` (`id`),
  ADD CONSTRAINT `fk_VooPcabine_n_voo` FOREIGN KEY (`n_voo`) REFERENCES `voo` (`n_voo`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
