# -*- coding: utf-8 -*-
"""
Created on Mon Jan  4 09:05:32 2021

@author: Bernardo
"""

import matplotlib.pyplot as plt
import numpy as np

x = np.linspace(-5,0,5)
x1 = 0
x2 = np.linspace(0,4,5)
y = x-3
y1 = 1
y2 = x+2
plt.plot(x,y,"-r",label = "y=x+2")
plt.plot(x1,y1,"-g",label = "y=1")
plt.plot(x2,y2,"-m",label= "y=x-3")
plt.title("Linear graphs")
plt.xlabel("x")
plt.ylabel("y")
plt.legend(loc="upper left")
plt.grid()
plt.show()

########################################

x = [-5,5]
m = [1,2,3,5]
b = [-2,3/4,2,1]
color = ["g", "r", "k", "m"]
for i in range (0,len(m)):
    y = m[i]*np.array(x) + b[i]
    plt.plot(x,y,label="y=%sx+%s" %(m[i],b[i]),color=color[i])
    
plt.axis("auto")
plt.xlim(x)
plt.ylim(x)
plt.grid()
plt.xlabel("x")
plt.ylabel("y")
plt.legend(prop={"size":15})
plt.title("Generic plot")
plt.show()

########################################

plt.hlines(y=3, xmin=1, xmax=10, colors="r")
plt.vlines(x=6, ymin=1, ymax=10, colors="r")
plt.title("Constant graphs")
plt.xlabel("x")
plt.ylabel("y")
plt.grid()
plt.show()

#########################################
#Exercises
#1
n = np.linspace(-8,8)                                       
f = n-5
g = 4*n+2
h = f+g                                             
plt.plot(n,f,"-r",label = "f = n-5")
plt.plot(n,g,"-b",label = "g = 4n+2")
plt.plot(n,h,"-g",label = "h = (f+g)")
plt.title("Exercise 1")
plt.xlabel("x")
plt.ylabel("y")
plt.legend(loc="upper left")
plt.grid()
plt.show()

#2
a = np.linspace(-10,2)
g = 3*a - 2
h = 4*a - 2
f = g+h                                                
plt.plot(a,g,"-r",label = "g = 3a - 2")
plt.plot(a,h,"-b",label = "h = 4a - 2")
plt.plot(a,f,"-g",label = "f = (g+h)(x)")
plt.title("Exercise 2")
plt.xlabel("x")
plt.ylabel("y")
plt.legend(loc="lower right")
plt.grid()
plt.show()

#3
x = np.linspace(-6,6) 
g = (x**2)-2
h = (2*x)+5
f = g+h                                      
plt.plot(x,g,"-r",label = "g = (x**2)-2")
plt.plot(x,h,"-b",label = "h = (2*x)+5")
plt.plot(x,f,"-g",label = "f = g(x)+h(x)")
plt.title("Exercise 3")
plt.xlabel("x")
plt.ylabel("y")
plt.legend(loc="lower right")
plt.grid()
plt.show()

#4
t = np.linspace(-6,6)
h = t+5
g = (3*t)-5
f = (h*g)                                                   
plt.plot(t,h,"-r",label = "h = t+h")
plt.plot(t,g,"-b",label = "g = (3*t)-5")
plt.plot(t,f,"-g",label = "f = (h*g)")
plt.title("Exercise 4")
plt.xlabel("x")
plt.ylabel("y")
plt.legend(loc="lower right")
plt.grid()
plt.show()

#5
n = np.linspace(-1,1)
h = (2*n)-1
g = (3*n)-5
f = h/g                                                 
plt.plot(n,h,"-r",label = "h = (2*n)-1")
plt.plot(n,g,"-b",label = "g = (3*n)-5")
plt.plot(n,f,"-g",label = "f = h/g")
plt.title("Exercise 5")
plt.xlabel("x")
plt.ylabel("y")
plt.legend(loc="lower right")
plt.grid()
plt.show()

#6
t = np.linspace(-2,2) 
g = t-3
h = (-3*(t**3))+(6*t)
f = g + h
plt.plot(t,g,"-r",label = "g = t-3")
plt.plot(t,h,"-b",label = "h = (-3*(t**3))+(6*t)")
plt.plot(t,f,"-g",label = "f = g + h")
plt.title("Exercise 6")
plt.xlabel("x")
plt.ylabel("y")
plt.legend(loc="upper left")
plt.grid()
plt.show()

#########################################

x = np.linspace(-10,10,10)
y1 = x**2
plt.plot(x,y1,"-r")
plt.grid()
plt.show()


x = np.linspace(-10,10,10)
y1 = -3*x**3+6*x
plt.plot(x,y1,"-b")
plt.grid()
plt.show()


x = np.linspace(-10,10,10)
y1 = -3*x**4+6*x+2*x
plt.plot(x,y1,"-g")
plt.grid()
plt.show()

##########################################

x = [-3,-2,-1,0,1,2,3]
y = [8,4,2,0,2,4,8]
color = ["g","r","k","m","k","r","g"]
for i in range(0,len(x)):
    plt.plot(x[i],y[i],"o",color=color[i])
plt.title("Discrete function")
plt.grid()
plt.show()

##########################################

x=[1,2,3,5,6,7,8,9,10,12,13,14,15,16,18,
   19,21,22]
y=[100,90,80,60,60,55,60,65,70,70,75,76,
   78,79,90,99,99,100]
mymodel = np.poly1d(np.polyfit(x,y,8))
myline = np.linspace(1,22,100)
plt.scatter(x,y)
plt.plot(myline, mymodel(myline))
plt.grid()
plt.show()

##########################################

#Exercises - Linear Regression                          #to be finished

x=(2400,2650,2350,4950,3100,2500,5106,
         3100,2900,1750)
y=(41200,50100,52000,66000,44500,37700,
          73500,37500,56700,35600)

mymodel = np.poly1d(np.polyfit(x,y,6))
myline = np.linspace(1750,5106)
plt.scatter(x,y)
plt.plot(myline, mymodel(myline))
plt.title("Linear Regression")
plt.grid()
plt.show()

##########################################

x=[1,2,3,4,5]
y1=[1,2,3,4,5]
plt.plot(x, y1, "-b",label="y=x")
plt.title("Discrete function using lists")
plt.xlabel("x")
plt.ylabel("y")
plt.legend(loc="upper left")
plt.grid()
plt.show()


x=(1,2,3,4,5)
y1=(1,2,3,4,5)
plt.plot(x, y1,"or",label="y=x")
plt.plot(x, y1,"b",label="y=x")
plt.title("Discrete function using tuples")
plt.legend(loc="upper left")
plt.grid()
plt.show()


x1={"x":[1,2,3,4,5], "y1":[1,2,3,4,5]}
plt.plot(x, y1,"-b",label="y=x", data=x1)
plt.title("Discrete function using dictionaries")
plt.legend(loc="upper left")
plt.grid()
plt.show()

##########################################

#Exponentials

import math

print(5**2)
print(math.log(25,5))
print(np.sqrt(25))

#Exercises

print("The 1st equation equals to:",(3**2)/3)
print("The 2nd equation equals to: m**2")
print("The 3rd equation equals to: (4*x**2*y)/3")
print("The 4th equation equals to: 4*(x**10)*(y**14)")
print("The 5th equation equals to: 2*(x**17)*2*(y**16)")
print("The 6th equation equals to:",(3**4)/3)
print("The 7th equation equals to: (x*y**3)/4")
print("The 8th equation equals to: (y**2)/4")
print("The 9th equation equals to: 8*(u**18)*(v**6)")
print("The 10th equation equals to: 3*u*v")

##########################################

#Logarithms

print(np.log(np.e))
print(np.log10(10))
print(np.log2(2))

#Examples

print("The 1st equation can be rewritten to: '7**2=x'")
print("The 2nd equation can be rewritten to: '10**3=x+8'")
print("The 3rd equation can be rewritten to: '5**x=125'\n")

print("The 1st equation can be rewritten to: 'log2(16)=4'")
print("The 2nd equation can be rewritten to: 'log64(8)=1/2'")
print("The 3rd equation can be rewritten to: 'loge(54.60)=4'")

#Exercises

print("The 1st equation equals to:",np.round(0.6**(np.sqrt(3)),3))
print("The 2nd equation equals to:",np.round((np.e**3.2),3))
print("The 3rd equation equals to:",np.round((1.005)**400,3))
print("The 4th equation equals to: 3")
print("The 5th equation equals to:",np.log(1))
print("The 6th equation equals to:",np.round(np.log(np.sqrt(7)),3))
print("The 7th equation equals to: 5")
print("The 8th equation equals to: -4")
print("The 9th equation equals to:", np.log(np.e**(-2)))
print("The 10th equation equals to: 0.565")
print("The 11th equation equals to: -1")
print("The 12th equation equals to: 1.380")
print("The 13th equation equals to: 1+log(x)")
print("The 14th equation equals to: ln(x)+ln(y)-ln(z)")
print("The 15th equation equals to: 1+2*log4(x)")
print("The 16th equation equals to: log3(x-2)/2")
print("The 17th equation equals to: (log(3x)/2)-log(7)")

##########################################

#Radicals

#Exercises

print("The 1st radical can be simplified to: 6*np.sqrt(3)")
print("The 2nd radical can be simplified to: 56*np.sqrt(2)")
print("The 3rd radical can be simplified to: -21*np.sqrt(7)")
print("The 4th radical can be simplified to: 7*np.sqrt(7*b)")
print("The 5th radical can be simplified to: 10*n*np.sqrt(n)")
print("The 6th radical can be simplified to: 48*np.sqrt(2)")
print("The 7th radical can be simplified to: -112*np.sqrt(2)")
print("The 8th radical can be simplified to: 8*np.sqrt(3*n)")
print("The 9th radical can be simplified to: 14*v")
print("The 10th radical can be simplified to: 10*a*np.sqrt(2*a)")

##########################################

x = np.linspace(0,8,10)
y1 = (2**x)-3
plt.plot(x,y1,"-m")
plt.title("Exponential Function")
plt.grid()
plt.show()


x = np.linspace(0,100,10)
y1 = np.sqrt(x)
plt.plot(x,y1,"-m",label="y=sqrt(x)")
plt.legend(loc="upper left")
plt.title("Radical Graphs")
plt.grid()
plt.show()


x = np.linspace(0,100,10)
y1 = np.log(x)
plt.plot(x,y1,"-m",label="y=log(x)")
plt.legend(loc="upper left")
plt.title("Log Graph")
plt.grid()
plt.show()

##########################################

x = np.linspace(0,3,10)
y1 = 2**x
y2 = 3**x
y3 =(2**x)+4
y4 = (2**(x-1))

plt.plot(x,y1,"m",label="y1 = 2**x")
plt.plot(x,y2,"b",label="y2 = 3**x")
plt.plot(x,y3,"r",label="y3 = (2**x)+4")
plt.plot(x,y4,"g",label="y4 = (2**(x-1))")
plt.legend(loc="upper left")
plt.title("Exponential Functions")
plt.grid()
plt.show()



x = np.linspace(0,4,5)
y1 = np.sqrt(x)
y2 = np.log(x)
y3 = 2**x

plt.plot(x,y1,"m",label="y1 = np.sqrt(x)")
plt.plot(x,y2,"b",label="y2 = np.log(x)")
plt.plot(x,y3,"r",label="y3 = 2**x")
plt.legend(loc="upper left")
plt.title("Comparison graph (exp, rad and log)")
plt.grid()
plt.show()






