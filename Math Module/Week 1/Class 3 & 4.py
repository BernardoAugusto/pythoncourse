# -*- coding: utf-8 -*-
"""
Created on Wed Jan 6 08:59:32 2021

@author: Bernardo
"""

import numpy as np
import matplotlib.pyplot as plt

#Trigonometry

#Examples
print(13/np.sin(np.radians(24)))

print(5/np.cos(np.radians(52.3)))

print(np.tan(np.radians(71))*9)

print(7.6/np.cos(np.radians(63)))

x = np.arcsin(0.5)
y = np.degrees(x)
x = np.arccos(0.667)
y = np.degrees(x)

#Exercises
print(np.round(np.degrees(np.arccos(4/7)),1))

print(np.round(np.degrees(np.arctan(12/13)),1))

print(np.round(np.degrees(np.arctan(16/10)),2))

print(np.round(np.degrees(np.arctan(5.6/15.3)),1))

############################################

np.set_printoptions(precision=2)
np.set_printoptions(formatter={"float":"{:0.2f}".format})
a = np.array([0,30,45,60,90])  #most used angles in trigonometry
print("Sine of different angles:")
print(np.sin(a*np.pi/180), "\n")
print("Cosine of different angles:")
print(np.cos(a*np.pi/180), "\n")
print("Tangent of different angles:")
print(np.tan(a*np.pi/180), "\n")

###########################################

#Periodic functions

values = np.linspace(-(2*np.pi), 2*np.pi, 20)
cos_values = np.cos(values)
sin_values = np.sin(values)
plt.plot(cos_values, color="blue", marker="*")
plt.plot(sin_values, color="green", marker="o")
plt.show()


x = np.linspace(-2*np.pi, 2*np.pi, 40)
x1 = np.rad2deg(x)
y = np.cos(x)
plt.plot(x1,y)
plt.xlabel("degree")
plt.ylabel("cos(x)")
plt.show()

x = np.linspace(-2*np.pi, 2*np.pi, 40)
y = np.cos(x)
plt.plot(x,y)
plt.xlabel("radians")
plt.ylabel("cos(x)")
plt.show()

############################################

#Periodic functions - Exercises

y1 = 4 + (1/2)*np.sin(x)
A1 = 1/2   #amplitude
B1 = 1     #period
C1 = 0     #horizontal shift
D1 = 4     #vertical shift

y2 = (-4)*np.cos((3*x)-np.pi)
A2 = -4
B2 = 3
C2 = np.pi/3
D2 = 0

x = np.linspace(-2*np.pi, 2*np.pi, 90)
y1 = 4 + (1/2)*np.sin(x)
y2 = (-4)*np.cos((3*x)-np.pi)
plt.plot(x,y1,label="y1 = 4+(1/2).sin(x)")
plt.plot(x,y2,label="y2 = -4.cos(3x-pi)")
plt.xlabel("Radians")
plt.ylabel("Amplitude")
plt.legend(loc="lower right")
plt.show()


x1 = np.linspace(-2*np.pi, 2*np.pi, 40)
x = np.degrees(x1)
y1 = 4 + ((1/2)*np.sin(x))
y2 = (-4)*np.cos((3*x)-np.pi)
plt.plot(x,y1,label="y1 = 4+(1/2).sin(x)")
plt.plot(x,y2,label="y2 = -4.cos(3x-pi)")
plt.xlabel("Degrees")
plt.ylabel("Amplitude")
plt.legend(loc="lower right")
plt.show()


x = np.linspace(0, 4*np.pi,100)
y1 = 4+(np.sin(x)/2)
y2 = (-4)*np.cos((3*x)-np.pi)
plt.plot(x,y1,label="y1 = 4+(sin(x)/2)")
plt.plot(x,y2,label="y2 = -4*cos(3*x-pi)")
plt.xlabel("Samples")
plt.ylabel("Amplitude")
plt.legend(loc="lower right")
plt.show()

###########################################

x = np.linspace(-5,5,100)
y = np.linspace(-5,5,100)
X,Y = np.meshgrid(x,y)
f = (X**2)+(Y**2)-16
fig, ax = plt.subplots()
ax.contour(X,Y,f,[0])
plt.title("Circle Graph")
plt.xlabel("x")
plt.ylabel("y")
plt.grid()
plt.show()

f = ((X-1)**2)+((Y+3)**2)-16
fig, ax = plt.subplots()
ax.contour(X,Y,f,[0])
plt.title("Circle Graph")
plt.xlabel("x")
plt.ylabel("y")
plt.grid()
plt.show()

###########################################

#Sequences

x = 1.0
n = 50
data = []
for i in range(n):
    x1 = i/(i+1)
    
    if abs(x1-x) < 1.e-2:
        print("n={i}, result is {x}.".format(i=i,x=x))
        last_index = i
        break
    x = x1
    data.append([i,x])
    print(i,x)
else:
    print("No convergence within {n} iterations.".format(n=n))
    last_index = n
plt.scatter(*zip(*data))
plt.hlines(y=1, xmin=0, xmax=last_index)  
plt.xlim(0,last_index)  
plt.show()



x = 0
n = 50
data = []
for i in range(n):
    x1 = 0.5**i
    print(i,x)
    if abs(x1-x) < 1.e-3:
        print("n={i}, result is {x}.".format(i=i,x=x))
        last_index = i
        break
    x = x1
    data.append([i,x])
else:
    print("No convergence within {n} iterations.".format(n=n))
    last_index = n
plt.scatter(*zip(*data))
plt.hlines(y=1, xmin=0, xmax=last_index)  
plt.xlim(0,last_index)  
plt.show()


x = 0
n = 50
data = []
for i in range(n):
    x1 = 2**i
    print(i,x)
    if abs(x1-x) < 1.e-3:
        print("n={i}, result is {x}.".format(i=i,x=x))
        last_index = i
        break
    x = x1
    data.append([i,x])
else:
    print("No convergence within {n} iterations.".format(n=n))
    last_index = n
plt.scatter(*zip(*data))
plt.hlines(y=1, xmin=0, xmax=last_index)  
plt.xlim(0,last_index)  
plt.show()



