# -*- coding: utf-8 -*-

"""
@author: Bernardo

"""
import numpy as np
import matplotlib.pyplot as plt
import math as m

print(m.factorial(2))

print("For the 1st exercise, the graphs that represent functions are:",
      "The 1st, the 3rd and the 4th")
print("Evaluating f(-1) i can say: f(-1)=-1")
print("For f(x)=3: x=7")
print("Evaluating f(0) i can say: f(0)=1")
print("For f(x)=-3: x=-2 or x=2")

print("Given:\nf = (3*x)-2\ng = 5-(x**2)\nh = (-2*(x**2))+(3*x)-1\n",
      "1.","The equation '3f(1)-4g(-2)' equals to: -1\n",
      "2.","The equation 'f(7/3)-h(-2)' equals to: 20")


#Exercises

x = np.linspace(-2*np.pi, 2*np.pi, 40)
y = (x**4) + 3
g = (x**5) + (4*x)
plt.plot(x,g,color="green",label="g = (x**5) + (4*x")
plt.plot(x,y,color="red",label="y = x**4 + 3")
plt.legend(loc="lower right")
plt.grid()
plt.show()
h = np.sqrt(x) + 6
j = np.cbrt(x)
plt.plot(x,j,color="#FFAEBC",label="j = np.cbrt(x)")
plt.plot(x,h,color="#A0E7E5",label="h = np.sqrt(x) + 6")
plt.legend(loc="lower right")
plt.grid()
plt.show()
k = np.tan(x)
plt.plot(x,k,color="purple",label="k = np.tan(x)")
plt.legend(loc="lower right")
plt.grid()
plt.show()
p = -x + 2
l = ((-3)*x) + 2
plt.plot(x,l,color="#FFAEBC",label="l = ((-3)*x) + 2")
plt.plot(x,p,color="orange",label="p = -x + 2")
plt.legend(loc="lower right")
plt.grid()
plt.show()
r = ((-2)*(x**2)) + 1
t = np.sin(2*x) + 4
plt.plot(x,t,color="yellow",label="t = np.sin(2*x) + 4")
plt.plot(x,r,color="#A0E7E5",label="r = ((-2)*(x**2)) + 1")
plt.legend(loc="lower right")
plt.grid()
plt.show()



#a)
x = np.linspace(0,20, 7)
y = 2*x
plt.plot(x,y,"og",label="y = 2x")
for i_x, i_y in zip(x, y):
    plt.text(i_x, i_y, '({}, {})'.format(int(i_x), int(i_y)))
plt.legend(loc="lower right")
plt.grid()
plt.show()
print("a) an = 2n; S5 = 2.5")

#b) 11
x = np.linspace(-1,10, 6)
y = (2*x)-1
plt.plot(x,y,"or",label="y = 2x-1")
plt.legend(loc="lower right")
plt.grid()
plt.show()
print("b) an = 2n-1; S5 = 2777/2340")

#c) 
x = np.linspace(-1,600, 7)
y = (x * 100)-1
plt.plot(x,y,"o", color="purple",label="y = 100x-1")
plt.legend(loc="lower right")
plt.grid()
plt.show()
print("c) an = 100n-1; S5 = -(67/8)")

#d) 
x = np.linspace(1, 100, 10)
y = ((-1)**(x+1))*(2*x+1)
plt.plot(x,y,"o", color="purple")
plt.grid()
plt.show()
print("d) an = ((-1)**(n+1))*(2*n+1); S5 = 65.814")

#e) 
x = np.linspace(1,(8/7),7)
y = (x+1)/x
plt.plot(x,y,"o", color="purple",label="y = (x+1)/x")
plt.legend(loc="lower left")
plt.grid()
plt.show()
print("e) an = (n+1)/n; S5 = 13.927")

#f) 
x = np.linspace(0, 38, 5)
y = x**2
plt.plot(x,y,"o", color="purple",label="y = x**2")
plt.legend(loc="lower right")
plt.grid()
plt.show()
print("f) an = n**2")

#g) 
x = np.linspace(0, 31, 6)
y = x*(x-1)
plt.plot(x,y,"o", color="purple",label="y = x*(x-1)")
plt.legend(loc="lower right")
plt.grid()
plt.show()
print("g) an = n*(n-1)")

#h) 
x = np.linspace((3/2), (32/243), 7)
y = (2/3)**(x-1)
plt.plot(x,y,"o", color="purple",label="y = (2/3)**(x-1)")
plt.legend(loc="lower left")
plt.grid()
plt.show()
print("h) an = (2/3)**(n-1)")

#i) 
x = np.linspace(6,60,7)
y = (x+1)*(x+2)
plt.plot(x,y,"o", color="purple",label="y = (x+1)*(x+2)")
plt.legend(loc="lower right")
plt.grid()
plt.show()
print("i) an = (n+1)*(n+2)")

#j) 
x = np.linspace(0,1,7)
y = (x+1)/(x*(x+2))
plt.plot(x,y,"o", color="purple",label="y = (x+1)/(x*(x+2))")
plt.legend(loc="lower left")
plt.grid()
plt.show()
print("j) an = (n+1)/(n*(n+2))")

#k)
x = np.linspace(0,6,7)
y = (1+(-1)**x)/6
plt.plot(x,y,"o", color="purple")
plt.grid()
plt.show()
print("k) an = (1+(-1)**n)/6")

#l)
x = np.linspace(1,100,10)
y = ((-1)**x)*(x/((3*x)-1))
plt.plot(x,y,"o", color="purple",label="y = ((-1)**x)*(x/((3*x)-1))")
plt.grid()
plt.legend(loc="lower right")
plt.show()
print("l) an = ((-1)**n)*(n/((3*n)-1))")
