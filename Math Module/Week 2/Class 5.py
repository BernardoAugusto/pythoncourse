# -*- coding: utf-8 -*-

"""
@author: Bernardo
"""

import numpy as np
from numpy import random
import matplotlib.pyplot as plt
from scipy.stats import uniform
from scipy.stats import expon
from scipy.stats import norm
import seaborn as sns
import random as rd

x = np.linspace(-10,10,100)
y = x
plt.hist(y,100,density=1,stacked=None)
plt.grid()
plt.show()
plt.plot(x,y,"o")
plt.grid()
plt.show()


data = uniform.rvs(size = 100000, loc = 5, scale=10)
plt.hist(data,bins=30)
plt.show()


x = np.linspace(-10,10,100)
y = x**2
plt.hist(y,100,density=1)
plt.show()
plt.plot(x,y,"o")
plt.show()


x = np.linspace(0.001,10,100)
pdf = expon.pdf(x)
plt.plot(x,pdf,"r-",lw=2,alpha=0.6,label="expon pdf")
plt.xlabel("intervals")
plt.ylabel("Probability Density")
plt.grid()
plt.show()


x = np.linspace(-10,10,100)
y = x**3
plt.hist(y,100,density=1)
plt.grid()
plt.show()
plt.plot(x,y,"o")
plt.grid()
plt.show()


n = 1000
x = np.linspace(0,6*np.pi,n)
sigl = np.sin(x)
plt.hist(sigl,100)
plt.grid()
plt.show()
plt.plot(x,sigl,"o")
plt.grid()
plt.show()


x = random.normal(loc=1,scale=2,size=1000)
plt.hist(x)
plt.grid()
plt.show()


data=np.random.normal(loc=0,size=1000)
mean,std=norm.fit(data)
plt.hist(data,bins=30,density=True)
xmin,xmax = plt.xlim()
x = np.linspace(xmin,xmax,100)
y = norm.pdf(x,mean,std)
plt.plot(x,y)
plt.show()


def random_samples(population,sample_gty,sample_size):
    sample_means=[]
    for i in range(sample_gty):
        sample = rd.sample(population,sample_size)
        sample_mean = np.array(sample).mean()
        sample_means.append(sample_mean)
    return sample_means
uniform = np.random.rand(100000)
print("mean =",np.mean(uniform))
plt.figure(figsize=(7,4))
plt.title("Distribution",fontsize=15)
plt.hist(uniform,100)

samples_from_normal = random_samples(list(uniform),10,10)
plt.figure()
plt.title("Distribution Results",fontsize=15)
sns.distplot(samples_from_normal,hist=False)