# -*- coding: utf-8 -*-

"""
@author: Bernardo
"""

from scipy import stats
from scipy.stats import gmean
from scipy.stats import hmean
import numpy as np
import random
import matplotlib.pyplot as plt

#Statistics
systematic_sample = []
sample = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15]
step = 2
print(np.mean(sample))
size = int(len(sample)/step)
for i in range(0,size):
    systematic_sample.append(sample[i*step])
systematic_mean = np.mean(systematic_sample)
print("Systemic sample:",systematic_mean)


systematic_sample = []
sample = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15]
step = 3
print(np.mean(sample))
size = int(len(sample)/step)
for i in range(0,size):
    systematic_sample.append(sample[i*step])
    systematic_sample.append(sample[i*step]+1)
systematic_mean = np.mean(systematic_sample)
print("Cluster sample:",systematic_mean)



def split_list(alist, wanted_parts=1):
    length = len(alist)
    return [ alist [i*length // wanted_parts: (i+1) *length
                    // wanted_parts]
            for i in range(wanted_parts)]
systematic_sample = []
sample = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15]
step = 3
A, B, C = split_list(sample,3)
total = split_list(sample,3)
stratified_list = []
print("mean all values:", np.mean(sample))

splitted_list = [A, B, C]
for i in range(0,len(splitted_list)):
    stratified_list.append(
        splitted_list[i][random.randint(0,4)])

systematic_mean = np.mean(stratified_list)
print("Stratified mean:",systematic_mean)

##########################################################

numbers = [1,1,2,2,3,3,4,1,2]
print(numbers)
Mean = np.mean(numbers)
Median = np.median(numbers)
Mode = int(stats.mode(numbers)[0])
print("Mean = ",Mean, "\nMedian = ",Median,
      "\nMode = ",Mode)

Geometric_Mean = gmean(numbers)
Harmonic_Mean = hmean(numbers)
print("Geometric Mean = ",Geometric_Mean,
      "\nHarmonic Mean = ",Harmonic_Mean)

Variance = np.var(numbers)
Standard_Deviation = np.std(numbers)
print("Variance = ",Variance,
      "\nStandard Deviation = ",Standard_Deviation)

Q1 = np.percentile(numbers, 25)
Q2 = np.percentile(numbers, 50)
Q3 = np.percentile(numbers, 75)
print("Q1 = ",Q1,
      "\nQ2 = ",Q2,
      "\nQ3 = ",Q3 )

Sample = [1,2,3,4,5,6,7,8,9,10,11,12]
print("With List:", random.sample(Sample, 5))

#Exercises
#1
print("\nExercise 1:")
numbers = [4,7,2,9,12,2,20,10,5,9]
Median = np.median(numbers)
Q1 = np.percentile(numbers, 25)
Q2 = np.percentile(numbers, 75)
iqr = Q2-Q1
print(numbers,
      "\nMedian = ",Median,
      "\nQ1 = ",Q1,
      "\nQ2 = ",Q2,
      "\nInter-quartile range = ",iqr )
#2
print("\nExercise 2:")
numbers = [2,5,10,11]
Mean = np.mean(numbers)
Variance = np.var(numbers)
Standard_Deviation = np.std(numbers)
print(numbers,
      "\nMean = ",Mean,
      "\nVariance = ",Variance,
      "\nStandard Deviation = ",Standard_Deviation)
#3
print("\nExercise 3:")
values = [4,1,2,1,3,2,1,1]
Median = np.median(values)
print("Median = ",Median)
#4
print("\nExercise 4:")
values = [4,1,2,1,3,2,1,1]
Standard_Deviation = np.std(values)
print("Standard Deviation = ",Standard_Deviation)

#############################################################

#Probability
simulation_number = 10000
club=13; spade=13; diamond=13;heart=13;percentages=[]
for i in range(0,simulation_number):
    card_points = [1,12,11,10,2,3,4,5,6,7,8,9,10]
    card_signs = ["Heart","Club","Diamond","Spade"]
    random_point = random.choice(card_points)
    random_sign = random.choice(card_signs)
    random_card = random_point,random_sign
    
    if ((random_card[1]=="Heart") or (random_card[1]=="Diamond") or
        (random_card[0]==1 and random_card[1]=="Spade") or
        (random_card[0]==1 and random_card[1]=="Club")):
        club+=1;
percentages.append(np.round(club/simulation_number*100,2))
fig = plt.figure()
ax = fig.add_axes([0,0,1,1])
ax.bar(card_signs,percentages)
plt.show()
print("\n",sum(percentages),"%")


#Exercises
#1
print("\nExercise 1:")
coin = ["Head","Tail"]
dice = [1,2,3,4,5,6]
both = coin + dice
print("Coin:",coin,"\nDice:",dice,"\nSum:",both)

#2
print("\nExercise 2:")
value = 1
p = value/len(dice) 
print("The probability of getting a 4 is:",np.round(p*100),"%")

#3
print("\nExercise 3:")
value = 0
simulation_number = len(dice)**2
percentage = []
outcomes = []
for i in range(len(dice)):
    for j in range(len(dice)):        
        dice1 = dice[i]
        dice2 = dice[j]
        x = dice1,dice2
        outcomes.append(x)
        if sum(x)>7 or sum(x)%2== 1:
            value += 1
percentage.append(np.round((value/simulation_number)*100,2))
print("The probability of the sum being >7 or odd is:",percentage[0],'%')

#4
print("\nExercise 4:")
value = 0
simulation_number = 10000
for i in range(0,simulation_number):
    dice1 = random.choice(dice)
    dice2 = random.choice(dice)
    random_match = dice1,dice2
    
    if (sum(random_match))>7 or (sum(random_match)%2) == 0:
        value += 1
percentage= (np.round((value/simulation_number)*100))
print("The probability of the sum being >7 or even is:",percentage,"%")

#5
print("\nExercise 5:")
value = 1
p = value/len(coin)**2
print("The probability of getting 2 heads is:",np.round(p*100),"%")

















