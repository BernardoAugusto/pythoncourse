# -*- coding: utf-8 -*-

"""
@author: Bernardo
"""

import numpy as np
import matplotlib.pyplot as plt

x = np.linspace(0,10,10)
y = x
plt.plot(x,y)
plt.scatter(x,y)
plt.grid()
plt.show()

x = np.linspace(0,10,10)
y = x
plt.plot(x,y)
plt.scatter(x,y)
plt.semilogy()
plt.grid()
plt.show()

x = np.linspace(0,10,10)
y = x
plt.plot(x,y)
plt.scatter(x,y)
plt.semilogx()
plt.grid()
plt.show()

x = np.linspace(0,10,10)
y = x
plt.plot(x,y)
plt.scatter(x,y)
plt.yscale('log')
plt.xscale('log')
plt.grid()
plt.show()

##############################################

#Exercises

#a)
N0 = 100
e = np.e
t =  np.linspace(1,12,10)
N = N0*(e**(2*t))
plt.plot(t,N,color="purple",label="N = N0*(e**(2*t))")
if np.all(N == np.log10(6)):
    plt.plot(N,"or")
plt.legend(loc="upper left")
plt.yscale('log')
plt.xlabel("Months")
plt.ylabel("Population")
plt.grid()
plt.show()


#b)
    #i)
print("-> Given the formula, with magnitude 5, the earthquake released is:",
      1.74*np.log10(19)*np.log10(1.44*5),"Joules")
    #ii)
#write plot



############################################

x = [2,30,70,100,150]
y = [4.24,16.4,25.1,30.0,36.7]
logx = np.log10(x)
logy = np.log10(y)
print(np.poly1d(np.polyfit(logx,logy,1)))
plt.plot(x,y,"-")
plt.show()

y1 = 3*np.sqrt(x)
plt.plot(x,y,"o")
plt.plot(x,y1,"-")
plt.show()

#Exercise    (3/2)
x = [5,15,30,50,95]
y = [10,90,360,1000,3610]
logx = np.log10(x)
logy = np.log10(y)
print(np.poly1d(np.polyfit(logx,logy,1)))
plt.plot(x,y,"-o")
plt.show()

y1 = (10**3)*(x**(3))
plt.plot(x,y,"o")
plt.plot(x,y1,"-")
plt.show()














