# -*- coding: utf-8 -*-

"""
@author: Bernardo
"""
import numpy as np
import matplotlib.pyplot as plt

#Matrices
print(np.array([2,3]))
print(np.matrix("2 3"))

print(np.array([[2],[4]]))
print(np.matrix("2; 4"))

x = [5,-7,3]
print(np.diag(x))

b = np.eye(3)
print(b)

x1 = np.array([[-1,3],[4,2]])
x2 = np.array([[1,2],[3,4]])
x3 = np.dot(x1,x2)
x4 = x1 + x2
x5 = x1 * x2
print(x3,"\n",x4,"\n",x5)

a = [[0,3],[4,0]]
print(np.linalg.det(a))
inverse_array = np.linalg.inv(a)
print(inverse_array)

#Cramer's Method
A1 = np.matrix("1 2;6 -3")
A2 = np.matrix("3 2;8 -3")
A3 = np.matrix("1 3;6 8")
det1 = np.linalg.det(A1)
det2 = np.linalg.det(A2)
det3 = np.linalg.det(A3)
print("x=",det2/det1)
print("y=",det3/det1)

#Matrices Inverses
A = np.matrix("1 2; 6 -3")
B = np.array([3,8])
X = np.linalg.inv(A).dot(B)
print("x,y =",X)

##################################################

l= []; l1= []

fig,ax = plt.subplots(3,1)
M = np.array([[1,3,-5,2],[0,4,-2,1],
              [2,-1,3,-1],[1,1,1,1]])
b = np.array([0,6,5,10])
l.append(b)
x = np.linalg.solve(M,b)
print(x)
l1.append(x)
ax[0].axis("off")
ax[0].imshow(M)
ax[1].axis("off")
ax[1].imshow(l)
ax[2].axis("off")
ax[2].imshow(l1)
plt.show()

################################################

#Exercises
#1
A1 = np.matrix("10 20;20 50")
A2 = np.matrix("1 20;0 50")
A3 = np.matrix("10 1;20 0")
det1 = np.linalg.det(A1)
det2 = np.linalg.det(A2)
det3 = np.linalg.det(A3)
print("x=",np.round((det2/det1),2))
print("y=",np.round((det3/det1),2))
B2 = np.matrix("0 20;1 50")
B3 = np.matrix("10 1;20 1")
det1 = np.linalg.det(A1)
det2 = np.linalg.det(B2)
det3 = np.linalg.det(B3)
print("t=",np.round((det2/det1),2))
print("z=",np.round((det3/det1),2))

#2
A = np.matrix("0 0 0 2;0 0 3 0;0 4 0 0;5 0 0 0")
print(np.linalg.inv(A))
B = np.matrix("3 2 0 0;4 3 0 0;0 0 6 5;0 0 7 6")
print(np.linalg.inv(B))

#3
A = np.matrix("0 3;4 0")
print(np.linalg.inv(A))
B = np.matrix("2 0;4 2")
print(np.linalg.inv(B))
C = np.matrix("3 4;5 7")
print(np.linalg.inv(C))

#4
c = 5*np.eye(4)
print(np.linalg.inv(c))

d = 6*np.eye(5)
print(np.linalg.inv(d))

#5
x = np.linspace(0,10)
f = (np.e**((-x)/10))*np.sin(np.pi*x)
g = x*(np.e**((-x)/3))
plt.plot(x,f,color="yellow",label="f = (np.e**((-x)/10))*np.sin(np.pi*x)")
plt.plot(x,g,color="#A0E7E5",label="g = x*(np.e**((-x)/3))")
plt.legend(loc="lower right")
plt.title("Exercise 5")
plt.grid()
plt.show()

#6
M = np.random.rand(10,10)
print("\nM:",M)
plt.matshow(M)
plt.title("Exercise 6")
plt.show()


#7
r0 = [0.8,1.0,1.2]
for i in r0:
    x1 = np.linspace(0,2*np.pi,10000)
    r = i + np.cos(x1)
    x = r*np.cos(x1)
    y = r*np.sin(x1)
    plt.plot(x,y,color="purple",label=("r0=",i))
    plt.title("Exercise 7")
    plt.legend(loc="lower right")
    plt.show()
















