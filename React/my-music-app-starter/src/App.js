import { useState, useRef } from "react";
import data from "./data.js";
import Player from "./components/Player.js";
import Song from "./components/Song.js";
import "./styles/App.scss";
import Library from "./components/Library";
import Nav from "./components/Nav.js";

function App() {

  const[songs,setSongs] = useState(data());
  const[currentSong, setCurrentSong] = useState(songs[0]);
  const[isLibraryOpen, setIsLibraryOpen] = useState(false);
  const [isPlay, setIsPlaying] = useState(false);
  const audioRef = useRef(null);
  const OpenLibrary = (event) =>{
    setIsLibraryOpen(!isLibraryOpen)
  };
  return (
    <div className={`App ${isLibraryOpen ? "library-active":""}`}>
      <Nav OpenLibrary ={OpenLibrary} />
      <Song currentSong ={currentSong}/>
      <Player 
        currentSong={currentSong} 
        setCurrentSong={setCurrentSong} 
        songs={songs} 
        isPlay= {isPlay}
        setIsPlaying = {setIsPlaying}
        setSongs={setSongs}
        audioRef={audioRef} />
      <Library 
        songs={songs} 
        isLibraryOpen ={isLibraryOpen} 
        currentSong={currentSong} 
        setSongs={setSongs} 
        setCurrentSong={setCurrentSong} 
        isPlay= {isPlay}
        setIsPlaying = {setIsPlaying}
        audioRef={audioRef} />
    </div>
  );
}

export default App;
