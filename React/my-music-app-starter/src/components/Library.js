import LibrarySong from "./LibrarySong";

const Library = ({
        currentSong, 
        songs, 
        setCurrentSong, 
        setSongs, 
        isPlay,
        isLibraryOpen, 
        setIsPlaying, 
        audioRef}) => {
    return (
        <div className={`library ${isLibraryOpen ? "open" : ""}`}>
            <h2>🌌Library</h2>
            <div className="library-songs">
                {
                    songs.map((song) => {
                        return <LibrarySong
                            key={song.id} 
                            song={song} 
                            songs={songs} 
                            currentSong={currentSong} 
                            setCurrentSong={setCurrentSong}
                            setSongs={setSongs}
                            isPlay={isPlay}
                            setIsPlaying={setIsPlaying}
                            audioRef={audioRef} />
                    })
                }
            </div>
        </div>
    );
}

export default Library;