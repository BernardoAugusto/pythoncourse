const LibrarySong =({
        currentSong,
        song, 
        songs, 
        setCurrentSong, 
        setSongs, 
        isPlay, 
        setIsPlaying, 
        audioRef}) => {
    const onSongSelect = async (event) => {
        const id = song.id;
        
        const newSongs = songs.map((stateSong) => {
            if(id === stateSong.id) {
                return {
                    ...song,
                    active: true
                }
            } else {
                return {
                    ...stateSong,
                    active: false
                }
            }
        });
        setSongs(newSongs);
        await setCurrentSong(song);
        setIsPlaying(true);
        audioRef.current.play();
    }
    return(
    <div className={`library-song ${song.active ? "selected" : ""}`} onClick={()=> onSongSelect(true)}>
        <img src={song.cover} alt={`${song.name}`}/>
        <div className="song-decription">
            <h3>{song.name}</h3>
            <h4>{song.artist}</h4>
        </div>
    </div>
    );
}

export default LibrarySong;