const Nav = ({OpenLibrary}) => {
    return(
        <nav>
            <h1>🪐My Music App</h1>
            <button onClick = {OpenLibrary}>
            Library 🎵
            </button>
        </nav>
    )
}

export default Nav;