import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import { faPlayCircle, faPauseCircle, faAngleDoubleLeft, faAngleDoubleRight, faRocket} from "@fortawesome/free-solid-svg-icons";
import { useRef, useState } from "react";

const Player = ({
        currentSong, 
        songs, 
        setCurrentSong, 
        setSongs, 
        isPlay, 
        setIsPlaying, 
        audioRef}) => {
    const animateTrackRef = useRef(null);
    const [songInfo, setSongInfo] = useState({
        current: 0,
        duration: 0
    });
    const animationPercentage = (songInfo.current/songInfo.duration)*100;
    
    const onAudioPlay = (event) => {
        if(isPlay){
            audioRef.current.pause();
            setIsPlaying(false);
        }else{
            audioRef.current.play();
            setIsPlaying(true);
        }
    }
    const onTimeUpdate = (event) =>{
        const current = event.target.currentTime;
        const duration = event.target.duration;
/*        animateTrackRef.current.style = {
            transform : `translateX(${Math.round((current/duration)*100)}%)`
        }*/
        setSongInfo({
            current: current,
            duration: duration
        })
    }
    const onTimeChange = (event) => {
        audioRef.current.currentTime = event.target.value
        setSongInfo({...songInfo, currentTime:event.target.value})
    }
    const skipTrack = async (forwards) => {
        const songsLength = songs.length;
        const currentSongIndex = songs.findIndex((song) => {
            return song.id === currentSong.id;
        })
        let nextIndex;
        if (forwards){
            nextIndex = (currentSongIndex+1) % songsLength
        } else{
            nextIndex = (songsLength + (currentSongIndex-1)) % songsLength
        }
        await setCurrentSong(songs[nextIndex]);
        notifyLibrary(songs[nextIndex]);
        setIsPlaying(true);
        audioRef.current.play();
    }
    const onEnded = async (event) => {
        //get next song
        const songCount = songs.length
        const currentSongIndex = songs.findIndex((song) => {
            return song.id === currentSong.id;
        })
        const nextIndex = (currentSongIndex + 1) % songCount;
        //play next song
        await setCurrentSong(songs[nextIndex]);
        notifyLibrary(songs[nextIndex]);
        setIsPlaying(true);
        audioRef.current.play();
    }
    const notifyLibrary = (nextPrev) => {
        const newSongs = songs.map((song) =>{
            if(song.id === nextPrev.id) {
                return {
                    ...song,
                    active: true
                }
            }else {
                return {
                    ...song,
                    active: false
                }
            }
        });
        setSongs(newSongs);
    }
    const getTime = (time) =>{
        return(
            Math.floor(time/60)+":"+("0"+Math.floor(time%60)).slice(-2)
        )
    }
    return (
        <div className="player">
            <div className="time-control">
                <p>{getTime(songInfo.current)}</p>
                <div className="track" style = {{ 
                    backgroundImage: `linear-gradient(
                    ${currentSong.color[0]},${currentSong.color[1]},${currentSong.color[2]},${currentSong.color[3]})`
                }} >
                    <input 
                        type="range" 
                        max={songInfo.duration} min={0} 
                        value={songInfo.current} 
                        onChange={onTimeChange} />
                    <div 
                        className="animate-track" 
                        ref={animateTrackRef} 
                        style={{
                            transform: `translateX(${animationPercentage}%)`
                        }} >
                        <div className="animate-track-thumb">
                            <FontAwesomeIcon className="rocket" icon={faRocket} />
                        </div>
                    </div>
                </div>
                <p>{getTime(songInfo.duration)}</p>
            </div>
            <div className="play-control">
                <FontAwesomeIcon icon={faAngleDoubleLeft} size="2x" onClick={()=> skipTrack(false)} />
                <FontAwesomeIcon icon={isPlay ? faPauseCircle : faPlayCircle} size="2x" onClick={onAudioPlay}/>
                <FontAwesomeIcon icon={faAngleDoubleRight} size="2x" onClick={()=> skipTrack(true)} />
                <audio 
                    src={currentSong.audio} 
                    ref={audioRef} 
                    onTimeUpdate={onTimeUpdate}
                    onLoadedMetadata={onTimeUpdate} 
                    onEnded={onEnded} />
            </div>
        </div>
    )
}

export default Player;