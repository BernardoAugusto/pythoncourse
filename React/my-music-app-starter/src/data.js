import { v4 as uuidv4 } from "uuid";

const Music = () => {
    return [
        {
            name: "Myth",
            cover:
                "https://f4.bcbits.com/img/a3016018801_10.jpg",
            artist: "Beach House",
            audio: "https://t4.bcbits.com/stream/f747ec8323221bab70b0f027e0dbcf7d/mp3-128/3711071170?p=0&ts=1609838780&t=c1bb9d93d237c6a115238c7b4c3303cdfbbb861b&token=1609838780_ba532b652ab4bc4574ce6edca3aef651d9c4d8dc",
            color: ["#080808","#C8C8C8","#7C7C7C","#A75E3A"],
            id: uuidv4(),
            active: true,
        },
        {
            name: "Seabirds",
            cover:
            "https://f4.bcbits.com/img/a0962419962_16.jpg",
            artist: "Pizzagirl",
            audio: "https://t4.bcbits.com/stream/06eadf14c0d4212d33fec153b631f09c/mp3-128/2683381235?p=0&ts=1609838981&t=e4f8ee5aead1aa1477a74f39290a20b7ed3de993&token=1609838981_7f344d6247d6cbe3a14ce6ce27323cc0e4e621ff",
            color: ["#CDABBE", "#224039","#766A49","#5F4725"],
            id: uuidv4(),
            active: false,
        },
        {
            name: "White Jeans",
            cover:
                "https://f4.bcbits.com/img/a2621287360_10.jpg",
            artist: "SALES",
            audio: "https://t4.bcbits.com/stream/f423e66c093c6bfc0b7e0ea4ef69baad/mp3-128/2770155854?p=0&ts=1609839178&t=2288db65a0364e6894bff64e54f3227579237307&token=1609839178_9be50b5c19de75c22c8c0461bdf1e8377d331bc4",
            color: ["#CD8F41","#E0DCD6","#AB592B","#302422"],
            id: uuidv4(),
            active: false,
        },
        {
            name: "Nobody",
            cover:
                "https://f4.bcbits.com/img/a2337835939_10.jpg",
            artist: "Mitski",
            audio: "https://t4.bcbits.com/stream/8abae1d84961a404594d29b89faf2b15/mp3-128/1340997060?p=0&ts=1609839266&t=26ad59dc1307bfb9b0b52eb12e2acaecdbf912ab&token=1609839266_093890bc1337c0144ac0af503b3c0150b09a60fc",
            color: ["#D9C9CD", "#312C29", "#8A4732", "#AF7557"],
            id: uuidv4(),
            active: false,
        },
        {
            name: "Summer Depression",
            cover:
                "https://f4.bcbits.com/img/a0241396390_16.jpg",
            artist: "girl in red",
            audio: "",
            color: ["#4A3D2B", "#4D7DC5","#DFB762","#A5B6C4"],
            id: uuidv4(),
            active: false,
        },
        {
            name: "Super 8 Tape",
            cover:
                "https://f4.bcbits.com/img/a0646781412_10.jpg",
            artist: "River Westin",
            audio: "",
            color: ["#2B3121", "#A75E3A","#C5DCCD","#839C8B"],
            id: uuidv4(),
            active: false,
        },
        {
            name: "I Was A Fool",
            cover:
                "https://f4.bcbits.com/img/a3045270451_10.jpg",
            artist: "Sunflower Bean",
            audio: "",
            color: ["#11181E","#C0A680","#848C82","#67778C"],
            id: uuidv4(),
            active: false,
        },
        {
            name: "Become The Warm Jets",
            cover:
            "https://f4.bcbits.com/img/a0685679618_10.jpg",
            artist: "Current Joys",
            audio: "",
            color: ["#E21414","#F2D8D6","#191818","#E37A8C"],
            id: uuidv4(),
            active: false,
        },
        {
            name: "Prisioner",
            cover:
            "https://songslover.vip/wp-content/uploads/Miley-Cyrus-Prisoner-feat.-Dua-Lipa.jpg",
            artist: "Miley Cyrus feat. Dua Lipa",
            audio: "",
            color: ["#D72F76","#909094","#8C7E8E","#26101C"],
            id: uuidv4(),
            active: false,
        },
        {
            name: "Midnight Sky",
            cover:
            "https://songslover.vip/wp-content/uploads/Miley-Cyrus-Midnight-Sky.jpeg",
            artist: "Miley Cyrus",
            audio: "",
            color: ["#BF9F96","#21141F","#7E949D","#796B8F"],
            id: uuidv4(),
            active: false,
        },
        {
            name: "911",
            cover:
            "https://songslover.vip/wp-content/uploads/Lady-Gaga-Chromatica.jpg",
            artist: "Lady Gaga",
            audio: "",
            color: ["#372B2F","#B95464","#C195AF","#C79B7B"],
            id: uuidv4(),
            active: false,
        },
        {
            name: "Matches",
            cover:
            "https://songslover.vip/wp-content/uploads/Britney-Spears-Glory-Deluxe-2020-1024x1024.jpg",
            artist: "Britney Spears",
            audio: "",
            color: ["#BEB6A5","#5490B8","#5D4532","#93653D"],
            id: uuidv4(),
            active: false,
        },
        {
            name: "Easy",
            cover:
            "https://songslover.vip/wp-content/uploads/Troye-Sivan-In-A-Dream-EP.jpg",
            artist: "Troye Sivan",
            audio: "",
            color: ["#145C9A","#0F1420","#811C24","#396C84"],
            id: uuidv4(),
            active: false,
        }
    ]
}

export default Music