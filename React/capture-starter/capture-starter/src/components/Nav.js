import styled from "styled-components";
import {Link} from 'react-router-dom';

const Nav = () => {
    return(
        <StyledNav>
            <h1>
                <link id="logo" to="/">
                    Capture
                </link>
            </h1>
            <ul>
                <li>
                    <link to="/">
                    1. About Us
                </link></li>
                <li>
                    <link to="/work">
                    2. Our Work
                </link></li>
                <li>
                    <link to="/contact">
                    3. Contact Us
                </link></li>
            </ul>
        </StyledNav>
    );
}

const StyledNav = styled.nav`
    display: flex;
    min-height: 10vh;
    margin: auto;
    justify-content: center;
    align-items: center;
    padding: 1rem 10rem;
    background-color: #282828;
    a{
        color: white;
        text-decoration: none;
    }
    ul{
        display:flex;
        list-style: none;
        margin-left: auto;
    }
    #logo{
        font-size: 1.5rem;
        font-family: "Lobster", cursive;
        font-weight: lighter;
    }
    li{
        padding-left: 10rem;
        position: relative;
    }
`

export default Nav;