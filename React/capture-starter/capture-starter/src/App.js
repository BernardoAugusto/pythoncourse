import AboutUs from "./pages/AboutUs";
import GlobalStyles from "./components/GlobalStyles"
import Nav from "./components/Nav";
import { Switch, Route} from "react-router-dom";
import OurWork from "./pages/OurWork";
import ContactUs from "./pages/ContactUs";
import MovieDetail from "./pages/MovieDetail";

function App() {
  return (
    <div className="App">
      <Nav />
      <GlobalStyles />
      <Switch>
        <Route path="/" exact>
          <AboutUs />
        </Route>
        <Route path="/work" exact>
          <OurWork />
        </Route>
        <Route path="/contact" exact>
          <ContactUs />
        </Route>
        <Route path="/work/:id" >
          <MovieDetail />
        </Route>
      </Switch>
    </div>
  );
}

export default App;