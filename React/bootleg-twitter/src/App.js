import { useState, useRef } from "react";
import TweetList from "./components/TweetList";
import { uuid } from 'uuidv4';


function App() {
const inputRef = useRef(null)

  const[submit, setSubmit] = useState("submit");

  const [tweets, setTweets] = useState([]);

//  useEffect(() => {   
//  },[tweets]);

  const submitClickHandler = (event) => {
    event.preventDefault();
    setSubmit("Submitted");
    const tweetObject = {
      id: uuid(),
      text: inputRef.current.value,
      liked: false
    }
    inputRef.current.value = "";
    const newTweets = [...tweets, tweetObject]
    setTweets(newTweets)
  }

  const appStyles = {
    color: "teal",
    backgroundColor: "teal"
  }
  return( <div className="App" style={appStyles}>
    <form>
      <input type="text" placeholder="Tweet" ref={inputRef}/>
      <button onClick={submitClickHandler} type="submit">{submit}</button>
    </form>
    <TweetList color="pink" tweets={tweets} setTweets={setTweets} />
  </div>);
}

export default App;
